local M = {}

function M:peek()
    local output, code = Command("soxi")
        :args({
            tostring(self.file.url),
        })
        :stdout(Command.PIPED)
        :output()

    local p
    if output then
        p = ui.Paragraph.parse(self.area, "----- Sox Information -----\n\n" .. output.stdout)
    else
        p = ui.Paragraph(self.area, {
            ui.Line {
                ui.Span("Failed to spawn `soxi` command, error code: " .. tostring(code)),
            },
        })
    end

    ya.preview_widgets(self, { p:wrap(ui.Paragraph.WRAP) })
end

return M
