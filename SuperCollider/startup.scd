var fixPorts = {
	// for unixy systems
	// windows will require a rewrite,
	// parsing the pipe result differently
	// I'm not going to do that today
	var pipe = Pipe("fuser 57120/udp", "r");
	var line;
	var pids = List.new;
	var cond = CondVar.new;
	var ok, allOK = true;
	protect {
		while {
			line = pipe.getLine.postln;
			line.notNil
		} {
			line.split($ ).do { |id|
				if(id.asInteger != 0) {
					pids.add(id)
				};
			};
		};
	} { pipe.close };
	fork {
		pids.do { |id|
			ok = false;
			"kill -9 %".format(id).unixCmd { |exit|
				if(exit == 0) {
					ok = true
				};
				cond.signalOne;
			};
			cond.waitFor(1);
			if(ok.not) {
				"Failed to stop pid %".format(id).warn;
			};
			allOK = allOK and: ok;
		};
		if(allOK) { 0.exit } { "Some processes didn't die".warn };
	};
};

if(Platform.ideName == "scqt" and: { NetAddr.langPort != 57120 }) {
	"Language port is %, networking may fail".format(NetAddr.langPort).warn;
	"Attempting to force-quit leftover processes\nIf successful, reboot the interpreter".postln;
	fixPorts.value;
};

s.options.numPrivateAudioBusChannels = 2048;
s.options.maxSynthDefs = 50000;
s.options.memSize = 2**20;
//make sure there are enough buffers
s.options.numBuffers = 2048;
//make sure there are enough internal buffers
s.options.numWireBufs = 10000;
s.options.maxNodes = 2**12;
// s.options.blockSize = 512;
s.latency  = 0.1;
// Always activate scnvim statusline
if (\SCNvim.asClass.notNil) {
    Server.default.doWhenBooted {
        \SCNvim.asClass.updateStatusLine(1, \SCNvim.asClass.port);
    }
};

Japa.framework = \jack;

Fosc.lilypondPath = "/usr/bin/lilypond";
