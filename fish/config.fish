if status is-interactive
    # Commands to run in interactive sessions can go here
end

source ~/.config/fish/envvars.fish
source ~/.config/fish/aliases.fish

set -U fish_greeting

# fish_vi_key_bindings

eval (starship init fish)

zoxide init fish | source

if status is-login
    and status is-interactive
    set -Ua SSH_KEYS_TO_AUTOLOAD ~/.ssh/id_rsa
    # Could also be an array of keys...
    # To remove a key, set -U --erase 
    # keychain --eval $SSH_KEYS_TO_AUTOLOAD | source
    eval (ssh-agent -c)
    ssh-add
end
