set -gx VISUAL "nvim"
set -gx EDITOR "nvim"
set -gx DIFFPROG "nvim"
set -gx PAGER nvimpager
set -gx MANPAGER nvimpager

# set -gx TERM xterm-256color
# set -gx TERM xterm-kitty
set -gx XDG_CACHE_HOME ~/.cache
set -gx TZ 'Europe/Tallinn'

set -gx QT_QPA_PLATFORMTHEME qt5ct

if pgrep sway > /dev/null
    set -gx MOZ_ENABLE_WAYLAND 1
    set -gx QT_QPA_PLATFORM wayland
    set -gx XDG_SESSION_TYPE wayland
    set -gx XDG_CURRENT_DESKTOP sway
    set -gx QT_WAYLAND_DISABLE_WINDOWDECORATION 1 
    set -gx QT_SCALE_FACTOR 1
end

set -gx LANG en_US.UTF-8
set -gx LC_PAPER nb_NO.UTF-8
set -gx PAPERSIZE a4
set -gx OPENWEATHERMAP_PLACE "Pärnu,Estonia"

set -gx PATH "$PATH:/home/kf/.gem/ruby/3.0.0/bin:/home/kf/.gem/ruby/2.7.0/bin:/home/kf/.gem/ruby/2.6.0/bin:/home/kf/.gem/ruby/2.5.0/bin:/home/kf/bin:/usr/lib/python2.7:/usr/lib/python3.7:/home/kf/go/bin:/home/kf/of:/home/kf/bin/fuzzy-yogurt:/home/kf/bin/rofi-jack:/home/kf/.local/bin:/home/kf/.cargo/bin:/opt/resolve/"

set -gx PG_OF_PATH {{pg_of_path}}
set -gx OF_ROOT $PG_OF_PATH

set -gx BROWSER '/usr/bin/firefox'
set -gx MATES_DIR ~/.contacts/woelkli/contacts 

set -gx FZF_DEFAULT_OPTS '--layout reverse'

set -gx GITLAB_TOKEN '/home/kf/glab-token'

set -gx fisher_path '/home/kf/dots/fish/'
