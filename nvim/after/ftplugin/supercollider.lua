vim.opt.shiftwidth=4
vim.opt.tabstop=4
vim.opt.expandtab = true
MiniPairs.map_buf(0, 'i', "|", { action = 'open', pair = '||', neigh_pattern = '\r.', register = { cr = false } })
