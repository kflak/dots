require("cmp_dictionary").setup({
    paths = {
        "/home/kf/.local/share/snd/keywords.txt",
    },
    exact_length = 2,
    first_case_insensitive = true,
    document = {
        enable = true,
        command = { "wn", "${label}", "-over" },
    },
})
