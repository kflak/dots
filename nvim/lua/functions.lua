local M = {}

local api = vim.api

function M.fzf(sources, sinkfunc)
	local fzf_run = vim.fn["fzf#run"]
	local fzf_wrap = vim.fn["fzf#wrap"]

	local wrapped = fzf_wrap("test", {
		source = sources,
		options = {'--reverse'},
		-- don't set `sink` or `sink*` here
	})
	wrapped["sink*"] = nil   -- this line is required if you want to use `sink` only
	wrapped.sink = sinkfunc
	fzf_run(wrapped)
end

-- function M.insert_text(text)
-- 	vim.api.nvim_paste(text, true, 1)
-- end

function M.shell(command)
	local lines = {}
    local file = io.popen(command)

    for line in file:lines() do
        table.insert(lines, line)
    end

    file:close()

    return lines
end
