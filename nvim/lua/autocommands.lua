local autocmd = vim.api.nvim_create_autocmd
-- local group = api.nvim_create_augroup("MyAutocmds", { clear = true})
local opt = vim.opt

autocmd({ "BufWritePost" }, {
    callback = function()
        require("lint").try_lint()
    end,
})

autocmd("FileType", {
    pattern = {"man"},
    callback = function()
        vim.keymap.set('n', 'q', '<cmd>qa!<cr>', { silent = true, nowait = true })
    end,
})

autocmd("FileType", {
    pattern = {"markdown"},
    callback = function()
        local file = vim.fn.expand("%:t"):gsub("%..*", "")
        vim.keymap.set('n', '<F5>', function() vim.cmd("!zathura " .. file .. ".pdf &") end, {})
        require('section-wordcount').wordcounter{}
    end,
})

autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*.md", "*.schelp", ".org"},
    callback = function()
        opt.textwidth = 72
        opt.formatprg = 'par'
        opt.foldlevel    = 0
        opt.conceallevel = 0
        opt.syntax = "markdown"
    end,
})

autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*.frag", "*.vert"},
    callback = function()
        vim.opt.filetype = "glsl"
    end,
})

autocmd({"VimLeavePre"}, {
    pattern = {"*.sc", "*.scd"},
    callback = function()
        require'scnvim'.send_silent('SerialPort.closeAll;')
    end,
})

autocmd({"TextYankPost"}, {
    callback = function() require'settings'.highlight() end,
})

-- autocmd({"TermOpen"}, {
--     callback = function() require'settings'.term_settings() end,
-- })

-- autocmd({"BufEnter", "BufWinEnter"}, {
--     pattern = {"*.c", "*.cpp", "*.h", "*.hpp", "*.lua", "*.html", "*.css", "*.sh", "*.vim"},
--     callback = function() require'mappings'.lsp() end,
-- })

-- autocmd({"BufEnter", "BufWinEnter"},
-- {
--     pattern = {"*.c", "*.cpp"},
--     callback = function()
--         require'mappings'.lsp()
--     end
-- })

autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*.c", "*.cpp", "*.h", "*.hpp"},
    callback = function() opt.makeprg='make -j$(nproc)' end,
})

autocmd({"BufEnter", "BufWinEnter"}, {
    pattern = {"*.html", "*.css", "json"},
    callback = function() require'settings'.web() end,
})

autocmd({"BufEnter"}, {
    pattern = {"*.ly"},
    command = "setlocal formatoptions-=c formatoptions-=r formatoptions-=o | syntax sync fromstart",
})

autocmd({ "BufWritePost" }, {
    callback = function()
        require("lint").try_lint()
    end,
})

autocmd('LspAttach', {
    group = vim.api.nvim_create_augroup('UserLspConfig', {}),
    callback = function(ev)
        -- Enable completion triggered by <c-x><c-o>
        vim.bo[ev.buf].omnifunc = 'v:lua.vim.lsp.omnifunc'

        -- Buffer local mappings.
        -- See `:help vim.lsp.*` for documentation on any of the below functions
        local opts = { buffer = ev.buf }
        vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, opts)
        vim.keymap.set('n', 'gd', vim.lsp.buf.definition, opts)
        vim.keymap.set('n', 'K', vim.lsp.buf.hover, opts)
        vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, opts)
        vim.keymap.set('n', '<C-k>', vim.lsp.buf.code_action, opts)
        vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, opts)
        vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, opts)
        vim.keymap.set('n', 'gr', vim.lsp.buf.references, opts)
        vim.keymap.set('n', '<space>f', function()
            vim.lsp.buf.format { async = true }
        end, opts)
        vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
        vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
        vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
    end,
})

autocmd({"BufReadPre", "BufNewFile"}, {
    pattern = "*",
    callback = function()
        require("lazy_plugins.lspconfig")
        -- require("lazy_plugins.nvim-cmp")
        return true
    end,
})

autocmd("FileType", {
    pattern = {"clojure"},
    -- TODO: Break this out to separate module
    callback = function()
        local function get_first_terminal()
            local terminal_chans = {}

            for _, chan in pairs(vim.api.nvim_list_chans()) do
                if chan["mode"] == "terminal" and chan["pty"] ~= "" then
                    table.insert(terminal_chans, chan)
                end
            end

            table.sort(terminal_chans, function(left, right)
                return left["buffer"] < right["buffer"]
            end)

            return terminal_chans[1]["id"]
        end

        local function terminal_send(text)
            local first_terminal_chan = get_first_terminal()

            vim.api.nvim_chan_send(first_terminal_chan, text)
        end

        vim.keymap.set('n', '<C-Y>', function()
            vim.cmd("ConjureEval (overtone.live/odoc " ..  vim.fn.expand("<cword>") .. ")")
        end, {})
        vim.keymap.set('n', '<F2>', function()
            require('toggleterm').toggle()
            terminal_send("clj -M:repl/conjure\n")
        end, {})

        -- vim.o.formatprg = "cljfmt"
    end,
})
