vim.g.nvlime_config = {
    cmp = { enabled = true },
    autodoc = {
        enabled = false,
        max_level = 5,
        max_lines = 50
    },
}
