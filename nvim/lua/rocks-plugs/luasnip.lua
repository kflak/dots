local set = vim.keymap.set;

set('n', '<leader>s', ':lua require("luasnip.loaders").edit_snippet_files()<CR>')
set('n', '<leader>r', ':lua require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/lua/snippets"})<CR>')
