vim.g.codeium_filetypes = {
    mail = false,
    lilypond = false,
    org = false,
    norg = false,
    markdown = false,
}
vim.g.codeium_no_map_tab = true
vim.keymap.set('i', '<C-i>', function () return vim.fn['codeium#Accept']() end, { expr = true })
vim.keymap.set('i', '<C-;>', function () return vim.fn['codeium#CycleCompletions'](1) end, { expr = true })
vim.keymap.set('i', '<C-,>', function () return vim.fn['codeium#CycleCompletions'](-1) end, { expr = true })
