local lsp = require'lspconfig'

lsp.marksman.setup{ }
lsp.texlab.setup{}
lsp.bashls.setup{ }
lsp.vimls.setup{ }
lsp.jsonls.setup { }
lsp.yamlls.setup { }
lsp.clojure_lsp.setup { }
lsp.lua_ls.setup{
    -- cmd = { "/usr/bin/lua-language-server", "-E", "/usr/bin/lua-language-server/main.lua"},
    settings = {
        Lua = {
            runtime = {
                version = 'LuaJIT',
                path = vim.split(package.path, ';'),
            },
            diagnostics = {
                globals = {
                    "vim",
                    "use",
                    "luasnip",
                    "has_words_before"
                },
                workspace = {
                    library = vim.api.nvim_get_runtime_file("", true),
                },
                telemetry = {
                    enable = false,
                },
            }
        }
    }
}
lsp.clangd.setup{

}
lsp.yamlls.setup{ }
lsp.pylsp.setup{}
vim.lsp.set_log_level("off")
-- lsp.set_log_level("debug") -- uncommented this when needed...
-- 'https://git.sr.ht/~p00f/clangd_extensions.nvim/'
require'clangd_extensions'.setup{}
-- require.'clangd_extensions.inlay_hints'.setup_autocmd{}
-- require.'clangd_extensions.inlay_hints'.set_inlay_hints{}
-- vim.keymap.set('n', '<leader>a', ':ClangdSwitchSourceHeader<cr>', {})
vim.api.nvim_create_user_command('A', 'ClangdSwitchSourceHeader', {})
