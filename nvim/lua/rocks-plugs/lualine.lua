local function scstatus()
    if vim.bo.filetype == "supercollider" then
        local status = vim.api.nvim_call_function("scnvim#statusline#server_status", {})
        status = status:gsub("%%", " ")
        return status
    else
        return ""
    end
end

local function codeiumstatus()
    local status = vim.api.nvim_call_function("codeium#GetStatusString", {})
    return "Codeium:" .. status or status
end
local function wordcount()
    local enable_filetype = {
        ["text"] = true,
        ["markdown"] = true,
        ["neorg"] = true,
    }
    local ft_check = enable_filetype[vim.bo.filetype]
    if ft_check ~= nil and ft_check ~= false then
        if vim.fn.wordcount().visual_words ~= nil then
            return "wc: " .. tostring(vim.fn.wordcount().visual_words) ..
                " ch: " .. tostring(vim.fn.wordcount().visual_chars)
        else
            return "wc: " .. tostring(vim.fn.wordcount().words) ..
                " ch: " .. tostring(vim.fn.wordcount().chars)
        end
    else
        return ""
    end
end

require('lualine').setup {
    theme = 'auto',
    sections = {
        lualine_b = { 'branch', 'diagnostics'},
        lualine_c = { '%=', {'filename', path = 1}, scstatus, codeiumstatus, wordcount },
        lualine_x = { 'filetype' },
    },
    options = { section_separators = '', component_separators = '' }
}

vim.opt.laststatus = 3

