require('nvls').setup({
    lilypond = {
        mappings = {
            player = "<F3>",
            compile = "<C-e>",
            open_pdf = "<F6>",
            switch_buffers = "<C-Space>",
            insert_version = "<F4>"
        },
        options = {
            pitches_language = "english",
            output = "pdf",
        },
    },
    latex = {
        mappings = {
            compile = "<F5>",
            open_pdf = "<F6>",
            lilypond_syntax = "<F3>"
        },
        options = {
            clean_logs = true
        },
    },
    player = {
        mappings = {
            quit = "q",
            play_pause = "p",
            loop = "<A-l>",
            backward = "h",
            small_backward = "<S-h>",
            forward = "l",
            small_forward = "<S-l>",
            decrease_speed = "j",
            increase_speed = "k",
            halve_speed = "<S-j>",
            double_speed = "<S-k>"
        },
        options = {
            row = "2%",
            col = "99%",
            width = "37",
            height = "1",
            border_style = "single",
            winhighlight = "Normal:Normal,FloatBorder:Normal",
            mpv_flags = {
                "--msg-level=cplayer=no,ffmpeg=no",
                "--audio-channels=stereo",
            },
            midi_synth = "fluidsynth",
            timidity_flags = nil,
        },
    },
})


local basepath = "/home/kf/.local/share/nvim/site/pack/rocks/start/nvim-lilypond-suite/lilywords/"

require("cmp_dictionary").setup({
    paths = {
        basepath .. "grobs",
        basepath .. "keywords",
        basepath .. "articulations",
        basepath .. "grobProperties",
        basepath .. "paperVariables",
        basepath .. "headerVariables",
        basepath .. "contextProperties",
        basepath .. "clefs",
        basepath .. "repeatTypes",
        basepath .. "languageNames",
        basepath .. "accidentalsStyles",
        basepath .. "scales",
        basepath .. "musicCommands",
        basepath .. "markupCommands",
        basepath .. "contextsCmd",
        basepath .. "dynamics",
        basepath .. "contexts",
        basepath .. "translators",
    },
    exact_length = 2,
    first_case_insensitive = true,
    document = {
        enable = true,
        command = { "wn", "${label}", "-over" },
    },
})

-- vim.g.LilypondLanguage = "nederlands"
