local overseer = require'overseer'
local set = vim.keymap.set
overseer.setup({
    templates = {},
})
overseer.register_template({
    name = "pandocpdfNumberSections",
    builder = function(params)
        return {
            cmd = {"pandoc", "-o", vim.fn.expand("%:p:t"):gsub(".md", "") .. ".pdf", "-i", vim.fn.expand("%:p:t"), "--template", "eisvogel", "--listings", "-V", "lang=en-US", "--number-sections"},
            name = "pandocpdf",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "Compile current file to PDF using pandoc with numbered sections",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"markdown"},
    },
})
overseer.register_template({
    name = "pandocpdf",
    builder = function(params)
        return {
            cmd = {"pandoc", "-o", vim.fn.expand("%:p:t"):gsub(".md", "") .. ".pdf", "-i", vim.fn.expand("%:p:t"), "--template", "eisvogel", "--listings", "-V", "lang=en-US"},
            name = "pandocpdf",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "Compile current file to PDF using pandoc",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"markdown"},
    },
})
overseer.register_template({
    name = "vimwiki2html",
    builder = function(params)
        return {
            cmd = {"pandoc", "-o", vim.fn.expand("%:p") .. ".html", "-i", vim.fn.expand("%:p"), "--self-contained", "--css=/home/kf/css/myStyle.css"},
            name = "vimwiki2html",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "Compile current file to html using pandoc",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"vimwiki"},
    },
})
overseer.register_template({
    name = "vimwiki2pdf",
    builder = function(params)
        return {
            cmd = {"pandoc", "-o", vim.fn.expand("%:p") .. ".pdf", "-i", vim.fn.expand("%:p"), "--template", "eisvogel", "--listings", "-V", "lang=en-US"},
            name = "vimwiki2pdf",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "Compile current file to PDF using pandoc",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"vimwiki"},
    },
})
overseer.register_template({
    name = "lilypond",
    builder = function(params)
        local file = require('nvls.config').fileInfos()
        local opts = require('nvls').get_nvls_options().lilypond.options

        local cmd = {
            "lilypond",
            "-f", opts.output,
        }

        local backend = opts.backend or nil
        if backend then
            table.insert(cmd, "-dbackend=" .. backend)
        end

        local include_dir = opts.include_dir or nil
        if type(include_dir) == "table" then
            for _, dir in ipairs(include_dir) do
                table.insert(cmd, "-I")
                table.insert(cmd, vim.fn.expand(dir))
            end
        elseif include_dir ~= nil and include_dir ~= "" then
            table.insert(cmd, "-I")
            table.insert(cmd, vim.fn.expand(include_dir))
        end

        table.insert(cmd, file.main)

        return {
            cmd = cmd,
            name = "lilypond",
            cwd = file.folder,
            components = {
                "default",
                {
                    "on_output_parse", parser = {
                        diagnostics = {
                            {
                                "extract",
                                "^([^%s].+):(%d+):(%d+): (.+)$",
                                "filename",
                                "lnum",
                                "col",
                                "text"
                            },
                        }
                    }
                },
                {
                    "on_result_diagnostics",
                    remove_on_restart = true,
                },
                {
                    "on_result_diagnostics_quickfix",
                    open = true
                },
                {
                    "on_complete_dispose",
                    timeout = 600
                },
            },
            metadata = {},
        }
    end,
    desc = "Compile Lilypond file",
    tags = { overseer.TAG.BUILD },
    priority = 50,
    condition = {
        filetype = { "lilypond" },
    },
})
overseer.register_template({
    name = "make run",
    builder = function(params)
        return {
            cmd = {"make", "run",},
            name = "make run",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "make run",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})
overseer.register_template({
    name = "compiledb make",
    builder = function(params)
        return {
            cmd = {"compiledb", "make",},
            name = "compiledb",
            cwd = "",
            env = {},
            components = {
                "default",
            },
            metadata = {},
        }
    end,
    desc = "compile database",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})
overseer.register_template({
    name = "make and run",
    builder = function(params)
        return {
            cmd = {"make", "-j8"},
            name = "make and run",
            cwd = "",
            env = {},
            components = {
                {"run_after", task_names = {"make run"}},
                -- {"on_complete_dispose", timeout = 1},
                "default",
            },
            metadata = {},
        }
    end,
    desc = "Compile project, rebuild compile_commands.json and run the app",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})
overseer.register_template({
    name = "make -j8",
    builder = function(params)
        return {
            cmd = {"make", "-j8",},
            name = "makeIt!",
            cwd = "",
            env = {},
            components = {
                {"run_after", task_names = {"compiledb make"}},
                "default",
            },
            metadata = {},
        }
    end,
    desc = "make with all cores",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})
overseer.register_template({
    name = "make debug -j8",
    builder = function(params)
        return {
            cmd = {"make", "-j8", "Debug",},
            name = "make debug",
            cwd = "",
            env = {},
            components = {
                {"run_after", task_names = {"compiledb make"}},
                "default",
            },
            metadata = {},
        }
    end,
    desc = "make debug with all cores",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})
overseer.register_template({
    name = "runIt",
    builder = function(params)
        return {
            cmd = {"make", "run"},
            name = "runIt",
            cwd = "",
            env = {},
            components = {
                {"on_complete_dispose", timeout = 3600},
                "default",
            },
            metadata = {},
        }
    end,
    desc = "run the app",
    tags = {overseer.TAG.BUILD},
    params = {},
    priority = 50,
    condition = {
        filetype = {"cpp", "h"},
    },
})

vim.keymap.set('n', '<leader>or', [[:OverseerRun<cr>]])
vim.keymap.set('n', '<leader>ow', [[:OverseerQuickAction watch<cr>]])
vim.keymap.set('n', '<leader>oo', [[:OverseerToggle<cr>]])


    -- {'<leader>or', [[:OverseerRun<cr>]]},
    -- {'<leader>ow', [[:OverseerQuickAction watch<cr>]]},
    -- {'<leader>os', [[:OverseerQuickAction restart<cr>]]},
    -- {'<leader>oq', [[:OverseerQuickAction<cr>]]},
    -- {'<leader>oo', [[:OverseerToggle<cr>]]},
