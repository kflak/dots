vim.g['conjure#filetype#scheme'] = 'conjure.client.snd-s7.stdio'
vim.g['conjure#highlight#enabled'] = true
vim.g['conjure#mapping#eval_root_form'] = '<space>'
