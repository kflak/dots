local t = require 'telescope'
t.setup {
    pickers = {
        find_files = {
            theme = "dropdown",
            no_ignore = true,
        }
    },
    defaults = {
        file_ignore_patterns = {
            "%.pdf$", "%.jpg$", "%.jpeg$", "%.png$", "%.gif$", "%.svg$", "%.wav$", "%.mp3$", "%.ogg$", "%.flac$",
            "%.m4a$", "%.mp4$", "%.webm$", "%.mov$", "%.o$", "%.so$"
        },
    }
}
t.load_extension('luasnip')
-- t.load_extension('dap')
t.load_extension('fzf')
