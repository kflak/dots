local wk = require("which-key")
vim.o.timeout = true
vim.o.timeoutlen = 500
wk.setup({
    wk.register({
        ["<c-p>"] = { "<cmd>Telescope find_files<cr>", "Find File" },
        ["<c-g>"] = { "<cmd>Telescope live_grep<cr>", "Live Grep" },
        ["<c-b>"] = { "<cmd>Telescope buffers<cr>", "Buffers" },
        ["<c-h>"] = { "<cmd>Telescope oldfiles<cr>", "History" },
        -- ["<c-s>"] = {[[<cmd>lua require'telescope'.extensions.luasnip.luasnip{}<CR>]], "LuaSnip"},
        ["<leader>gb"] = { "<cmd>lua require'telescope.builtin'.git_branches()<CR>", "Git Branches" },

        -- ["<leader>gg"] = {[[:Neogit<cr>]], "Neogit"},
        -- ["<leader>gc"] = {[[:Neogit commit<cr>]], "Neogit commit"},
        -- ["<leader>gp"] = {[[:Neogit push<cr>]], "Neogit push"},
        -- ["<leader>gf"] = {[[:Neogit diff<cr>]], "Neogit diff"},

        -- NOT working. Why?
        -- ["<c-i>"] = { function () return vim.fn['codeium#Accept']() end, 
        --     "Codeium Accept",
        --     { mode = "i", expr = true}
        -- },
        -- ["<c-;>"] = { function () return vim.fn['codeium#CycleCompletions'](1) end, 
        --     "Codeium Cycle +1",
        --     { mode = "i", expr = true}},
        -- ["<c-,>"] = { function ()
        --     return vim.fn['codeium#CycleCompletions'](-1) end,
        --     "Codeium Cycle -1", { mode = "i", expr = true}},
    }),
})
