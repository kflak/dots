local gl = require('galaxyline')
local gls = gl.section
-- local colors  = require('galaxyline.colors')

gl.short_line_list = {'LuaTree','vista','dbui'}

local colors = {
    bg = vim.g.terminal_color_0,
    grey = vim.g.terminal_color_8,
    white = vim.g.terminal_color_15,
    yellow = vim.g.terminal_color_3,
    cyan = vim.g.terminal_color_6,
    green = vim.g.terminal_color_2,
    purple = vim.g.terminal_color_5,
    blue = vim.g.terminal_color_4,
    red = vim.g.terminal_color_1,
}

local empty_buffer = function()
    if vim.fn.empty(vim.fn.expand('%:t')) ~= 1 then
        return true
    end
    return false
end

local checkwidth = function()
    local squeeze_width  = vim.fn.winwidth(0) / 2
    if squeeze_width > 40 then
        return true
    end
    return false
end

-- Custom file icon for atypical file types
local my_icons = require('galaxyline.provider_fileinfo').define_file_icon() -- get file icon color

-- scnvim:
my_icons['supercollider'] = { colors.blue, "✹" }
my_icons['help.supercollider'] = { colors.yellow, "✹" }
my_icons['scnvim'] = { colors.yellow, "✹" }

-- -- Arch linux
my_icons['PKGBUILD'] = { colors.red, "☡"}



gls.left[1] = {
    ViMode = {
        provider = function()
            local alias = {
                n = 'Normal',
                i = 'Insert',
                c = 'Command',
                v = 'Visual',
                [''] = 'Visual',
                t = 'Terminal'
            }
            local mode_color = {
                Normal = colors.blue,
                Insert = colors.green,
                Command = colors.red,
                Visual = colors.cyan,
                Terminal = colors.purple
            }

            -- Text for mode
            local current_mode = alias[vim.fn.mode()]

            -- Get color for mode
            local current_bg = mode_color[current_mode]
            local current_fg = colors.white

            -- Set color
            vim.cmd(string.format('hi GalaxyViMode guibg=%s guifg=%s', current_bg, current_fg))

            return current_mode 
        end,
        separator = ' ',
        separator_highlight = {colors.bg, colors.bg},
        highlight = {colors.bg,colors.green,'bold'},
    },
}

gls.left[2] = {
    FileIcon = {
        provider = 'FileIcon',
        condition = empty_buffer,
        separator = ' ',
        separator_highlight = {colors.bg, colors.bg},
        highlight = {require('galaxyline.provider_fileinfo').get_file_icon_color, colors.bg},
    },
}

gls.left[3] = {
    FileName = {
        provider = {'FileName'},
        condition = empty_buffer,
        separator = ' ',
        separator_highlight = {colors.bg, colors.bg},

        -- separator_highlight = {colors.purple,colors.blue},
        highlight = {colors.yellow, colors.bg}
    }
}

gls.left[4] = {
    GitBranch = {
        provider = {'GitBranch', 'DiffAdd', 'DiffRemove'},
        condition = empty_buffer,
        icon = '  ',
        separator = ' ',
        separator_highlight = {colors.bg, colors.bg},
        highlight = {colors.blue, colors.bg},
    }
}

gls.left[5] = {
    Scnvim = {
        provider = function()
            local scstatus = vim.api.nvim_call_function("scnvim#statusline#server_status", {})
            return scstatus
        end,
        condition = function()
            if vim.api.nvim_get_option("filetype") == "supercollider" then
                return true
            else
                return false
            end
        end,
        icon = '  ',
        separator = '',
        separator_highlight = {colors.blue, colors.bg},
        highlight = {colors.green, colors.bg},
    }}

    gls.right[1] = {
        FileType = {
            provider = function()
                return vim.api.nvim_get_option("filetype")
            end,
            -- condition = empty_buffer,
            separator = ' ',
            icon = ' ⌨ ',
            separator_highlight = {colors.bg, colors.bg},
            highlight = {colors.grey, colors.bg},
        },
    }

    gls.right[2] = {
        LineInfo = {
            provider = 'LineColumn',
            separator = ' ',
            separator_highlight = {colors.bg, colors.bg},
            highlight = {colors.grey,colors.bg},
        },
    }

    gls.right[3] = {
        PerCent = {
            provider = 'LinePercent',
            separator = ' ',
            separator_highlight = {colors.bg, colors.bg},
            highlight = {colors.yellow, colors.bg},
        }
    }
