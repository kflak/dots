local M = {}
local opt = vim.opt
local g = vim.g

function M.settings()
    M.options()
    M.window_options()
    M.netrw()
end

function M.options()

    opt.autowriteall   = true
    opt.breakindentopt = 'shift:1'
    opt.clipboard      = 'unnamedplus'
    opt.cmdheight      = 1
    opt.compatible     = false
    opt.completeopt    = 'menu,menuone,noselect'
    opt.diffopt        = 'filler,internal,algorithm:histogram,indent-heuristic'
    opt.encoding       = 'utf-8'
    opt.expandtab      = true
    opt.fillchars      = 'eob: '
    opt.formatprg      = 'fmt'
    opt.formatoptions  = 'jcroql'
    opt.gdefault       = true
    opt.history        = 200
    opt.hlsearch       = true
    opt.inccommand     = "split"
    opt.incsearch      = true
    opt.infercase      = true
    opt.joinspaces     = false
    opt.nrformats:append { 'alpha' }
    opt.path:append {"~/sc" }
    opt.shiftwidth     = 4
    opt.showbreak      = '↳'
    opt.ignorecase     = true
    opt.smartcase      = true
    opt.smarttab       = true
    opt.softtabstop    = 0
    opt.tabstop        = 4
    opt.splitright     = true
    opt.termguicolors  = true
    opt.textwidth      = 0
    opt.title          = true
    opt.undofile       = true
    opt.wildmode       = "full"
    opt.shortmess:append "cI"
    -- opt.shortmess:append "I"
    -- opt.background     = "dark"
    -- opt in on new lua filetype detection and disable filetype.vim
    -- g.do_filetype_lua = 1
    -- g.did_load_filetypes = 0
end

function M.window_options()
    opt.breakindent = true
    opt.linebreak   = true
    opt.number      = true
    opt.relativenumber = true
    opt.wrap        = true
end

function M.netrw()
    g.netrw_liststyle = 3
    g.netrw_banner    = 0
    g.netrw_winsize   = 25
end

function M.highlight()
    vim.highlight.on_yank{higroup="IncSearch", timeout=500}
end

function M.web()
    opt.tabstop     = 2
    opt.shiftwidth  = 2
    opt.softtabstop = 2
end

function M.glsl()
    opt.commentstring = '//%s'
end
return M
