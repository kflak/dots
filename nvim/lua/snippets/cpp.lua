-- Return file name without extension
local function fileNoExt()
    return vim.fn.expand("%:t"):gsub("%..*", "")
end

return{
    s("setupUpdateDrawHeader",
        fmt([[
    void setup();
    void update();
    void draw();
    ]], {})),
    s("setupUpdateDrawImpl",
        fmt([[
    #include "<>"

    void <>::setup(){
        <>
    }

    void <>::update(){
        <>
    }

    void <>::draw(){
        <>
    }
    ]], {
                i(1, ""),
                i(2, "ClassName"),
                i(3, ""),
                rep(2),
                i(4, ""),
                rep(2),
                i(5, ""),
            },
            {
                delimiters = "<>"
            })),
    s("getter", fmt([[inline auto {}() const {{ return {}; }};]], { i(1, "getter"), i(2, "val") })),
    s("lag",
        fmt([[
    {} = {} + {} * ({} - {});
    {} = {};
    ]],
            { i(1, "current"),
                rep(1),
                i(2, "lag"),
                i(3, "prev"),
                rep(1),
                rep(3),
                rep(1),
            })),
}
