return{
    s("it", {t([[\item]])}),
    s("a4",
    fmt(
    [[
    \documentclass[titlepage,a4paper,12pt]{{{}}}
    \usepackage{{newtxtext}}
    \usepackage{{graphicx}}
    \usepackage{{geometry}}
    \geometry{{margin=2cm}}
    \usepackage{{fancyhdr}}
    \usepackage[utf8]{{inputenc}}
    \usepackage[T1]{{fontenc}}
    \usepackage{{mathtools}}
    \pagestyle{{fancy}}
    \fancyhf{{}}
    \fancyhead[R]{{\thepage}}
    \setlength{{\headheight}}{{15pt}}
    \renewcommand{{\headrulewidth}}{{0pt}}

    \linespread{{{}}}

    \title{{{}}}
    \author{{{}}}
    \date{{{}}}

    \begin{{document}}
        {}
    \end{{document}}
    ]],
    {
        i(1, "article"),
        i(2, "1.25"),
        i(3, "title"),
        i(4, "author"),
        i(5, "\\today"),
        i(6, "document"),
    })
    ),
    s("beamer",
    fmt(
    [[
    \documentclass[aspectratio=169]{{beamer}}
    \usepackage[T1]{{fontenc}}
    \usetheme{{moloch}}

    \title{{{}}}
    \author{{{}}}
    \date{{{}}}

    \begin{{document}}

    \titlepage

    \begin{{frame}}{{{}}}
        {}
    \end{{frame}}

    \end{{document}}
    ]],
    {
        i(1, "title"),
        i(2, "Roosna \\& Flak"),
        i(3, "\\today"),
        i(4, "frametitle"),
        i(5, "")
    })
    ),
    s("frame",
    fmt(
    [[
    \begin{{frame}}{}{{{}}}
        {}
    \end{{frame}}
    ]],
    {
        c(1, {
            t("[fragile]"),
            t(""),
        }),
        i(2, "frametitle"),
        i(3, "")
    })
    ),
    s("verbatim",
    fmt(
    [[
    \begin{{verbatim}}
    {}
    \end{{verbatim}}
    ]],
    {
        i(1, "")
    }
    )),
    s("graphics", 
    fmt(
    [[
    \includegraphics{}{{{}}}
    ]],
    {
        c(1, {
            i(1, "[scale=0.5]"),
            t(""),
        }),
        i(2, "filename without extension"),
    })
    ),
}
