local date_input = function(args, snip, old_state, fmt)
    local fmt = fmt or "%Y-%m-%d"
    return sn(nil, i(1, os.date(fmt)))
end

return {
    s("doc",
        fmt(
            [[
            @document.meta
            title: <>
            description: <>
            author: <>
            created: <>
            @end
            ]]
            ,
            {
                i(1, ""),
                i(2, ""),
                i(3, ""),
                d(4, date_input, {})
            },
            {  delimiters = "<>"  }
        )
    ),
}
