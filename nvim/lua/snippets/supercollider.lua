return {
    s("NdefFilter", fmt(
        [[
        Ndef(\<>, Pdef(\<>)).play;
        Ndef(\<>).filter(2, {|in|
            <>
        })
        ]],
        {
            i(1, "name"),
            rep(1),
            rep(1),
            i(2, "")
        },
        {  delimiters = "<>"  }
    )),
    s("mbdeltatrig",
        fmt(
            [[
    {} = KF.mb.collect({{|i|
        MBDeltaTrig.new(
            speedlim: {}, 
            threshold: {},
            minibeeID: {},
            minAmp: {},
            maxAmp: {},
            function: {{|dt, minAmp, maxAmp, id|
                {}
            }}
        ).play;
    }});
    ]],
            {
                i(1, "variable"),
                i(2, "0.5"),
                i(3, "0.05"),
                i(4, "i"),
                i(5, "-70"),
                i(6, "0"),
                i(7, "function"),
            })),
    s("dmxcue",
        fmt(
            [[
    {} = DMXCue.new();
    ]],
            {
                i(1, "cuename"),
            })),
    s("hasEnttec", 
        fmt(
            [[
        if( ~dmx.notNil ){
            <>
        };
        ]],
            { i(1, "") },
            {  delimiters = "<>"  }
        )),
    s("pdef",
        fmt(
            [[
    Pdef(\{},
        {}(
            \{}, \{},
        )
    )
    ]],
            {
                i(1, "name"),
                i(2, "Pbind"),
                i(3, "instrument"),
                i(4, "sine"),
            })),
    s("pbind",
        fmt(
            [[
    Pbind(
        \{}, \{},
        \{}
    )
    ]],
            {
                i(1, "instrument"),
                i(2, "sine"),
                i(3),
            })),
    s("sdef",
        fmt(
            [[
    SynthDef(\{}, {{
            var sig, env{};
            {}
            {}
            sig = sig * env * \amp.kr({});
            Out.ar(\out.kr(0), sig);
    }}).add;
    ]],
            {
                i(1, "name"),
                i(2, ""),
                c(3, {
                    t([[env = EnvGen.kr(Env.asr( \attack.kr(0.01), 1, \release.kr(0.01)), gate: \gate.kr(1), doneAction: \da.kr(2));]]),
                    t([[env = EnvGen.kr(Env.perc(\attack.kr(0), \release.kr(1)), doneAction: 2);]])
                }),
                i(4, "sig = SinOsc.ar(\\freq.kr(440));"),
                i(5, ""),
            })),
    s("fxchain",
        fmt(
            [[
    {} = FxChain.new(
        fadeInTime: {},
        level: {},
        fadeOutTime: {},
        out: {},
        fadeInCurve: {}, 
        fadeOutCurve: {}
    )
    ]],
            {
                i(1, "name"),
                i(2, "1"),
                i(3, "0.dbamp"),
                i(4, "1"),
                i(5, "0"),
                i(6, "4"),
                i(7, "-4"),
            })),
    s("reverb",
        fmt(
            [[
    {}.add(\jpverb,
        \mix, {},
        \revtime, {},
    )
    ]],
            {
                i(1, "FxChainName"),
                i(2, "0.3"),
                i(3, "3"),
            })),
    s("class",
        fmt(
            [[
    {} {{
        *new{{|{}|
            ^super.new({}).init;
        }}

        init {{
            {}
        }}
    }}
    ]],
            {
                i(1, "Class : SuperClass"),
                i(2, "args"),
                i(3, "args"),
                i(4, "init"),
            })),
    s("setter",
        fmt(
            [[
    {}_{{|val| {} = val; {}}}
    ]],
            {
                i(1, "param"),
                d(2, function(args)
                    return sn(nil, {
                        i(1, args[1])
                    })
                end,
                    {1}),
                i(3),
            })),
    s("mix",
        fmt(
            [[
    sig = (1 - {}) * sig + ({} * {});
    ]],
            {
                i(1, "mix"),
                i(2, "wet"),
                d(3, function(args)
                    return sn(nil, {
                        i(1, args[1])
                    })
                end,
                    {1}),
            })),
    s("compressor",
        fmt(
            [[
    sig = Compander.ar(sig, sig, {}.dbamp, slopeAbove: 1/{});
    ]],
            {
                i(1, "threshold"),
                i(2, "ratio"),
            })),
    s("jpverbmono",
        fmt(
            [[
    verb = JPverbMono.ar({}, size: {}, t60: {});
    ]],
            {
                i(1, "in"),
                i(2, "1.0"),
                i(3, "1.0"),
            }
        )),
    s("jpverbstereo",
        fmt(
            [[
    verb = JPverb.ar({}, size: {}, t60: {});
    ]],
            {
                i(1, "in"),
                i(2, "1.0"),
                i(3, "1.0"),
            }
        )),
    s("prCreateFx",
        fmt(
            [[
    prCreateFx{{
        fx.source = {{
            var lag = \lag.kr(0.1);
            var in, sig, out;
            in = In.ar({}, KF.numSpeakers) * \inputgain.kr(0.dbamp);
            {}
            sig = BHiPass.ar(in: sig, freq: {});
            sig = BHiShelf.ar(in: sig, freq: {}, db: {});
            out = sig * \amp.kr({}.dbamp);
        }}
    }}
    ]],
            {
                i(1, "fxBus"),
                i(2, "sig = in;"),
                i(3, "40"),
                i(4, "800"),
                i(5, "0"),
                i(6, "0"),
            })),
    s("dtlinlin",
        fmt(
            [[
    dt.linlin(0.0, 1.0, {}, {})
    ]],
            {
                i(1),
                i(2),
            })),
    s("cueAdd",
        fmt(
            [[
    ~cue.add(
        timeline: [
            {}, {{
                {}
            }},
        ],
    );
    ]],
            {
                i(1, "0"),
                i(2),
            })),
    s("pmFBPbind",
        fmt(
            [[
Pbind(
    \instrument, \pmFB,
    \freq, <>,
    \modfreq, Pfunc{|ev| ev.use{~freq.()}},
    \modfeedback, <>,
    \pmindex, <>,
    \dur, <>,
    \attack, <>,
    \release, <>,
    \modattack, <>,
    \modrelease, <>,
    \db, <>,
    \pan, <>,
)
       ]],
            {
                i(1, "440"),
                i(2, "0.3"),
                i(3, "1"),
                i(4, "1"),
                i(5, [[Pkey(\dur)/2]]),
                i(6, [[Pkey(\dur)/2]]),
                i(7, [[Pkey(\attack) * 2]]),
                i(8, [[Pkey(\release)]]),
                i(9, "-6"),
                i(10, "Pwhite(-1.0, 1.0)"),
            },
            { delimiters = '<>'}
        )),
    s("granPbind", fmt(
        [[
Pbind(
    \instrument, \granulator,
    \buf, <>,
    \dur, <>,
    \attack, <>,
    \release, <>,
    \rate, <>,
    \tRateModFreq, <>,
    \tRateModDepth, <>,
    \rateModFreq, <>,
    \rateModDepth, <>,
    \posRate, <>,
    \posRateModFreq, <>,
    \posRateModDepth, <>,
    \pan, <>,
    \db, <>,
    \out, <>,
)
        ]],
        {
            i(1, "0"),
            i(2, "Pfunc{|ev| ev.buf.duration}"),
            i(3, "0.01"),
            i(4, "0.1"),
            i(5, "1"),
            i(6, "1"),
            i(7, "1"),
            i(8, "1"),
            i(9, "1"),
            i(10, "1"),
            i(11, "1"),
            i(12, "1"),
            i(13, "0"),
            i(14, "-6"),
            i(15, "0"),
        },
        {  delimiters = "<>"}
    )),
}
