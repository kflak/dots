return {
    s("tuplet",
       fmt([[\tuplet {} {{ {} }}]],
       { i(1, "3/2"), i(2, "")})
    ),
    s("inclRomanNumeral",
        t([[\include "/home/kf/lilypond/lilypond-roman-numeral-tool/roman_numeral_analysis_tool.ily"]])
    ),
    s("romanNumeral",
    fmt([[\markup \rN {{ {} }}]],
    { i(1, "")})),
    s("inclArrows",
        t([[\include "/home/kf/lilypond/my-funcs/arrow.ly"]])
    ),
    s("textmark",
        fmt([[\textMark "{}"]],
        { i(1, "text here")})),
    s("addlyrics",
    fmt(
    [[\addlyrics {{ {} }}]],
    { i(1, "lyrics here")})),
    s("markup",
    fmt(
    [[\markup {{ {} }}]],
    {
        i(1, "")
    })),
    s("key",
    fmt(
    [[\key {} \{}]],
    {
        i(1, "c"),
        c(2, {
            t("major"),
            t("minor"),
        })
    })),
    s("header",
    fmt(
    [[
    \header {{ 
        title = "{}" 
        composer = "{}"
        tagline = "{}"
    }}
    ]],
    {
        i(1, ""),
        i(2, "Kenneth Flak"),
        i(3, ""),
    })),
    s("midi", t([[\midi{}]])),
    s("layout",
    fmt(
    [[ 
    \layout {{
      \context {{
        \Score
        \numericTimeSignature
        \time {}
      }}
    }}
    ]],
    {
        i(1, [[4/4]]),
    })),
    s("bar",
    fmt(
    [[
    \bar {}
    ]],
    {
        c(1, {
            t([["|."]]),
            t([["||"]]),
        })
    })),
    s("lang", t([[\language english]])),
    s("pianostaff",
    fmt(
    [[
    right = {{
      \relative c'{{
      {}
      }}
    }}
    
    left = {{
      \relative c {{
        \clef bass
        {}
      }}
    }}

    \score {{
      \new PianoStaff
      <<
        \new Staff = "right" \right
        \new Staff = "left" \left
      >>
    }}
    ]],
    {
        i(1, ""),
        i(2, ""),
    })),
    s("satb",
    fmt(
    [[
    SopranoMusic = \relative c' {{
    {}
    }}
    SopranoLyrics = \lyricmode {{
    {}
    }}
    AltoMusic = \relative c' {{
    {}
    }}
    AltoLyrics = \lyricmode {{
    {}
    }}
    TenorMusic = \relative {{
    {}
    }}
    TenorLyrics = \lyricmode {{
    {}
    }}
    BassMusic = \relative {{
    {}
    }}
    BassLyrics = \lyricmode {{
    {}
    }}
    PianoRHMusic = \relative {{
    {}
    }}
    PianoDynamics = {{
    {}
    }}
    PianoLHMusic = \relative {{
    {}
    }}
    TwoVoicesPerStaff = ##{}
    \include "satb.ly"
    ]],
    {
        i(1, ""),
        i(2, ""),
        i(3, ""),
        i(4, ""),
        i(5, ""),
        i(6, ""),
        i(7, ""),
        i(8, ""),
        i(9, ""),
        i(10, ""),
        i(11, ""),
        i(12, "t"),
    })),
    s("satbBasic",
    fmt(
    [[
    SopranoMusic = \relative c' {{
      {}
      {}
      {}
    }}
    AltoMusic = \relative c' {{
      {}
    }}
    TenorMusic = \relative {{
      {}
    }}
    BassMusic = \relative {{
      {}
      {}
      {}
    }}
    TwoVoicesPerStaff = ##{}
    \include "satb.ly"
    ]],
    {
        i(1, "\\time 4/4"),
        i(2, [[\key c \major]]),
        i(3, ""),
        i(4, ""),
        i(5, ""),
        rep(1),
        rep(2),
        i(6, ""),
        i(7, "t"),
    })),
    s("24", {t("\\time 2/4")}),
    s("34", {t("\\time 3/4")}),
}
