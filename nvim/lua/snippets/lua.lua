return{

    -- Snippets for snippets
    s("snip", fmt(
        [[
        s("{}", {}),
        ]],
        { i(1, "name"), i(2, "") })),
    s("fmt", fmt(
        [[fmt(
        "{}",
        {{ {} }},
        {{ {} }}
        )]],
        { i(1, ""), i(2, ""), i(3, [[ delimiters = "<>" ]]) }
    )
    ),
    s("snipt", fmt( [[ s("{}", {{t({})}}) ]], { i(1, "name"), i(2, ""), })),
}
