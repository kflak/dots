local date_input = function(args, snip, old_state, fmt)
	local fmt = fmt or "%Y-%m-%d"
	return sn(nil, i(1, os.date(fmt)))
end

return {

    s("a4",
    fmt(
    [[
    ---
    geometry: a4paper
    title: {}
    author: {}
    titlepage: {}
    disable-header-and-footer: {}
    date: {}
    urlcolor: magenta
    ---
    ]],
    {
        i(1, "Insert title here"),
        i(2, "Kenneth Flak" ),
        i(3, "false" ),
        i(4, "false"),
        d(5, date_input, {})
    }
    )
    ),
    s("zolablog",
    fmt(
    [[
    +++
    title = "{}"
    date = "{}"

    [extra]
    author = "{}"
    +++
    ]],
    {
        i(1, "Insert title here"),
        d(2, date_input, {}),
        i(3, "Külli Roosna"),
    })
    )
}
