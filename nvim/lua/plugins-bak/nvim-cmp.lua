return {
    'hrsh7th/nvim-cmp',
    config = function()
        local cmp = require'cmp'
        local lspkind = require'lspkind'
        local ls = require("luasnip")
        lspkind.init()

        cmp.setup {
            snippet = {
                expand = function(args)
                    require('luasnip').lsp_expand(args.body)
                end,
            },
            mapping = cmp.mapping.preset.insert({
                ['<C-Space>'] = cmp.mapping.complete,
                ['<C-e>'] = cmp.mapping({
                    i = cmp.mapping.abort(),
                    c = cmp.mapping.close(),
                }),
                ['<C-l>'] = cmp.mapping.confirm({
                    select = true,
                }),
                ["<tab>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        cmp.select_next_item()
                    elseif luasnip.expand_or_jumpable() then
                        luasnip.expand_or_jump()
                    elseif has_words_before() then
                        cmp.complete()
                    else
                        fallback()
                    end
                end, { "i", "s" }),

                ["<S-Tab>"] = cmp.mapping(function(fallback)
                    if cmp.visible() then
                        cmp.select_prev_item()
                    elseif luasnip.jumpable(-1) then
                        luasnip.jump(-1)
                    else
                        fallback()
                    end
                end, { "i", "s" }),
            }),
            sources = {
                { name = 'luasnip' },
                { name = 'nvim_lsp' },
                { name = 'path', keyword_length = 1 },
                { name = 'tags' },
                { name = 'buffer' },
                { name = 'conjure'},
                { name = 'neorg'},
            },
            formatting = {
                format = lspkind.cmp_format {
                    with_text = true,
                    menu = {
                        luasnip = '[snip]',
                        nvim_lsp = '[LSP]',
                        dictionary = '[dict]',
                        path = '[path]',
                        tags = '[tags]',
                        buffer = '[buf]',
                    },
                },
            },
            completion = {
                keyword_length = 2,
            },
            view = {
                native_menu = true
            },
        }

        cmp.setup.filetype("lilypond", {
            sources = {
                { name = 'luasnip' },
                { name = 'buffer' },
                { name = 'dictionary' },
                { name = 'path', keyword_length = 1 },
            }
        })
        cmp.setup.filetype("tex", {
            sources = {
                { name = 'luasnip' },
                { name = 'vimtex' },
                { name = 'buffer' },
            }
        })
        cmp.setup.filetype({"lisp"}, {
            sources = {
                { name = 'nvlime' },
                { name = 'luasnip' },
                { name = 'path', keyword_length = 1 },
                { name = 'buffer' },
                -- other sources like path or buffer, etc.
                -- .
                -- .
            }
        })

        vim.keymap.set({"i", "s"}, "<c-l>", function() ls.jump( 1) end, {silent = true})
        vim.keymap.set({"i", "s"}, "<c-j>", function() ls.jump(-1) end, {silent = true})

        vim.keymap.set({"i", "s"}, "<c-e>", function()
            if ls.choice_active() then
                ls.change_choice(1)
            end
        end, {silent = true})

        require('luasnip.loaders.from_vscode').lazy_load()
        require("luasnip.loaders.from_lua").lazy_load({paths = "~/.config/nvim/lua/snippets"})
    end,
    dependencies = {
        'hrsh7th/cmp-buffer',
        'hrsh7th/cmp-nvim-lua',
        'hrsh7th/cmp-nvim-lsp',
        'hrsh7th/cmp-path',
        'quangnguyen30192/cmp-nvim-tags',
        'onsails/lspkind-nvim',
        'saadparwaiz1/cmp_luasnip',
        'lukas-reineke/cmp-rg',
        'hrsh7th/cmp-calc',
        'rafamadriz/friendly-snippets',
        'madskjeldgaard/vim-scdoc-snippets',
        'uga-rosa/cmp-dictionary',
        'micangl/cmp-vimtex',
        -- {
        --     'tzachar/cmp-tabnine',
        --     build = './install.sh',
        -- }
    }
}
