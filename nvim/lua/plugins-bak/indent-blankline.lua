return {
    'lukas-reineke/indent-blankline.nvim',
    init = function()
        vim.g.indent_blankline_show_first_indent_level = false
    end
}
