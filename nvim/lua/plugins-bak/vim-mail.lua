return {
    'https://gitlab.com/dbeniamine/vim-mail',
    -- 'https://gitlab.com/kflak/vim-mail',
    -- enabled = false,
    init = function()
        vim.g.VimMailContactsProvider = 'khard'
        -- vim.g.VimMailClient = "kitty neomutt -R"
        vim.g.VimMailStartFlags = {
            nosubject = "T",
            blank = "T",
            default = "toi"
        }
        vim.g.VimMailSpellLangs = {'en', 'no'}
    end,
    -- dependencies = {
    --     {
    --         'chrisbra/CheckAttach',
    --         init = function()
    --             vim.g.checkattach_once = 'y'
    --         end
    --     }
    -- },
    ft = 'mail'
}
