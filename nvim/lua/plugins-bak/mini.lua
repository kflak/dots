return {
    'echasnovski/mini.nvim',
    -- enabled = false,
    branch = 'stable',
    config = function()
        require'mini.pairs'.setup({ })
    end
}
