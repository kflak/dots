return{
    'mileszs/ack.vim',
    init = function()
        vim.g.ackprg = 'rg --vimgrep --type-not sql --smart-case'
        vim.g.ack_use_cword_for_empty_search = true
        vim.cmd([[cnoreabbrev Ack Ack!]])
    end
}
