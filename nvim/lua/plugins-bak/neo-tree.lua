return {
    'nvim-neo-tree/neo-tree.nvim',
    branch = 'v3.x',
    keys = {
        {'<leader>f', ':Neotree toggle<CR>'}
    }
}
