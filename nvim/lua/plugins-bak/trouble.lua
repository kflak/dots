return {
    'folke/trouble.nvim',
    keys = {
        { '<leader>xx', ':Trouble<CR>' },
        { '<leader>xw', ':Trouble workspace_diagnostics<CR>' },
        { '<leader>xd', ':Trouble document_diagnostics<CR>' },
        { '<leader>xl', ':Trouble loclist<CR>' },
        { '<leader>xq', ':Trouble quickfix<CR>' },
        { '<leader>xr', ':Trouble lsp_references<CR>' },
    }
}
