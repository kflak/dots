return {
    'nvim-treesitter/nvim-treesitter',
    config = function()
        require'nvim-treesitter.configs'.setup {
            ensure_installed =  {
                'bash',
                'c',
                'cmake',
                'comment',
                'commonlisp',
                'cpp',
                'css',
                'html',
                'javascript',
                'json',
                'lua',
                'make',
                'org',
                'python',
                'regex',
                'rust',
                'supercollider',
                'toml',
                'vim',
                'vimdoc',
                'yaml'
            }, -- could be 'all'   
            matchup = {
                enable = true,
            },
            sync_install = false,
            auto_install = true,
            -- ignore_install = { "commonlisp" },
            highlight = {
                enable = true,              -- false will disable the whole extension
                additional_vim_regex_highlighting = {'org'},
            },
            indent = {
                enable = {},
                disable = { 'supercollider' },
            },
            refactor = {
                smart_rename = {
                    enable = true,
                    keymaps = {
                        smart_rename = "grr",
                    },
                },
                navigation = {
                    enable = true,
                    -- Assign keymaps to false to disable them, e.g. `goto_definition = false`.
                    keymaps = {
                        goto_definition = "gnd",
                        list_definitions = "gnD",
                        list_definitions_toc = "gO",
                        goto_next_usage = "<a-*>",
                        goto_previous_usage = "<a-#>",
                    },
                },
                highlight_current_scope = { enable = true },
            },
        }
    end,
    dependencies = {
        'nvim-treesitter/nvim-treesitter-refactor',
        {
            'm-demare/hlargs.nvim',
            config = true,
            -- config = function()
            --     require'hlargs'.setup()
            -- end
        }
    },
}
