return {
    'tpope/vim-fugitive',
    enabled = false,
    keys = {
        {'mf', [[:diffget //2<cr]]},
        {'mj', [[:diffget //3<cr]]},
        {'mo', [[:Gvdiffsplit!<cr>]]},
    },
    lazy = false
}
