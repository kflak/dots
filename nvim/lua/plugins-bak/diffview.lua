return {
    'sindrets/diffview.nvim',
    keys = {
        {'<leader>dv', [[:DiffviewOpen<cr>]]}
    }
}
