return {
    'stevearc/aerial.nvim',
    config = function()
        require'aerial'.setup({
            on_attach = function(bufnr)
                vim.keymap.set('n', '<leader>ap', '<cmd>AerialPrev<CR>', {buffer = bufnr})
                vim.keymap.set('n', '<leader>an', '<cmd>AerialNext<CR>', {buffer = bufnr})
            end,
        })
    end,
    dependencies  ={
        'nvim-treesitter/nvim-treesitter',
    },
    keys = {
        {'<leader>a', [[:AerialToggle!<cr>]]},
        -- {'<leader>af', [[<cmd>call aerial#fzf()<cr>]]},
    }
}
