return {
    'davidgranstrom/scnvim',
    dependencies = {
        'davidgranstrom/osc.nvim',
        'madskjeldgaard/telescope-supercollider.nvim',
    },
    config = function()
        local scnvim = require'scnvim'
        local map = scnvim.map
        local map_expr = scnvim.map_expr

        scnvim.setup {
            keymaps = {
                -- ['<M-e>'] = map('editor.send_line', { 'i', 'n' }),
                ['<C-Space>'] = map('editor.send_line', { 'i', 'n' }),
                ['<Space>'] = {
                    map('editor.send_block', 'n'),
                    map('editor.send_selection', 'x'),
                },
                ['<F12>'] = map('sclang.hard_stop', { 'n', 'x', 'i' }),
                ['<CR>'] = map('postwin.toggle', 'n'),
                ['<C-;>'] = map('signature.show', { 'n', 'i' }),
                ['<C-y>'] = map(function() require 'telescope'.extensions.scdoc.scdoc() end),
                ['<F1>'] = map(function()
                    scnvim.send([[HelpBrowser.openHelpFor("]] .. vim.call('expand', '<cword>') .. [[")]]);
                end, { 'n', 'x', 'i' }, { desc = "help for word under cursor" }),
                ['<M-s>'] = map(function()
                    scnvim.send([[HelpBrowser.openSearchPage("]] .. vim.call('expand', '<cword>') .. [[")]]);
                end, { 'n', 'x', 'i' }, { desc = "search for word under cursor" }),
                ['<F2>'] = map('sclang.start'),
                ['<F3>'] = map(function()
                    require("luasnip").add_snippets("supercollider", require("scnvim/utils").get_snippets())
                end),
                ['<F4>'] = map(function()
                    scnvim.send('Server.killAll')
                    scnvim.recompile()
                end),
                ['<F5>'] = map(function()
                    scnvim.send('s.plotTree')
                    os.execute("hyprctl dispatch layoutmsg swapwithmaster")
                end),
                -- ['<F5>'], = map_expr('s.plotTree')]),
                ['<F6>'] = map_expr('s.meter'),
                ['<F7>'] = map_expr('Japa.new'),
                ['<F8>'] = scnvim.map(function()
                    scnvim.send(
                    'if(s.isRecording){s.stopRecording}{s.record("rec"+/+Git(thisProcess.nowExecutingPath.dirname).sha[0..5]++"_"++Date.localtime.stamp++".wav", numChannels: s.options.numOutputBusChannels)}')
                end),
                ['<F9>'] = map_expr('s.scope'),
                ["<F10>"] = scnvim.map(function()
                    require 'telescope'.extensions.supercollider.sc_definitions()
                end, { "n", "x", "i" }),
                ['<leader>d'] = map(function()
                    vim.cmd [[edit ~/.local/share/SuperCollider/Extensions/KF/Classes/KFSynthDefs.sc]]
                end)
            },
            editor = {
                highlight = {
                    color = 'IncSearch',
                    type = 'flash'
                }
            },
            documentation = {
                cmd = '/usr/bin/pandoc'
            },
            postwin = {
                size = 50
            },
            snippet = {
                engine = {
                    name = 'luasnip',
                },
                mul_add = false -- not yet implemented
            },
        }

        require'telescope'.load_extension('scdoc')
        require'telescope'.load_extension('supercollider')
        -- no longer works:
        -- vim.cmd[[autocmd filetype scnvim setlocal wrap]]
        -- workaround for the time being:
        require('scnvim.postwin').on_open:append(function()
            vim.opt_local.wrap = true
        end)
    end
}
