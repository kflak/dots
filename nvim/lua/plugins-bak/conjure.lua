return {
    -- 'russtoku/conjure',
    'Olical/conjure',
    ft = {'clojure', 'scheme'},
    -- enabled = false,
    init = function()
        vim.g['conjure#filetype#scheme'] = 'conjure.client.snd-s7.stdio'
        vim.g['conjure#highlight#enabled'] = true
        vim.g['conjure#mapping#eval_root_form'] = '<space>'


        -- local cond = require('nvim-autopairs.conds')
        -- require("nvim-autopairs").get_rules("'")[1].not_filetypes = { "clojure", "lilypond", "scheme", "lisp" }
        -- require("nvim-autopairs").get_rules("\"")[1].not_filetypes = { "clojure", "lilypond", "scheme", "lisp" }
        -- require("nvim-autopairs").get_rules("'")[1]:with_pair(cond.not_after_text("["))
    end,
    branch = 'develop',
    dependencies = {
        {'PaterJason/nvim-treesitter-sexp', config = true},
        'PaterJason/cmp-conjure',
        'dmac/vim-cljfmt',
        'gpanders/nvim-parinfer'
        -- 'guns/vim-sexp',
        -- 'tpope/vim-sexp-mappings-for-regular-people',
    },
}
