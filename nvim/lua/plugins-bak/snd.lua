return {
    'salkin-mada/snd.nvim',
    enabled = false,
    config = function()
        require('snd')
    end,
    init = function()
        vim.g.snd_keymaps = {exec = '<C-e>'}
        vim.g.snd_filetypes = {"*.scm"}
    end
}
