return {
    'mfussenegger/nvim-lint',
    config = function()
        require('lint').linters_by_ft = {
            -- cpp = {'cpplint'},
            markdown = {'markdownlint'},
            bash = {'shellcheck'},
            yaml = {'yamllint'},
            glsl = {'glslc',}
        }
    end
}
