return {
    'williamboman/mason.nvim',
    build = ":MasonUpdate", -- :MasonUpdate updates registry contents
    init = function()
        require('mason').setup()
    end,
}
