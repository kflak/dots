return {
    'nvim-telescope/telescope.nvim',
    enabled = true,
    version = '0.1.x',
    lazy = true,
    config = function()
        local t = require 'telescope'
        t.setup {
            pickers = {
                find_files = {
                    theme = "dropdown",
                    no_ignore = true,
                }
            },
            defaults = {
                file_ignore_patterns = {
                    "%.pdf$", "%.jpg$", "%.jpeg$", "%.png$", "%.gif$", "%.svg$", "%.wav$", "%.mp3$", "%.ogg$", "%.flac$",
                    "%.m4a$", "%.mp4$", "%.webm$", "%.mov$", "%.o$", "%.so$"
                },
            }
        }
        t.load_extension('luasnip')
        t.load_extension('dap')
        t.load_extension('fzf')
    end,
    dependencies = {
        'nvim-lua/plenary.nvim',
        'davidgranstrom/telescope-scdoc.nvim',
        'benfowler/telescope-luasnip.nvim',
        {
            'nvim-telescope/telescope-fzf-native.nvim', build = 'make'
        }
    },
    keys = {
        { '<c-p>', [[:Telescope find_files<cr>]] },
        { '<c-g>', [[:Telescope live_grep<cr>]] },
        { '<c-b>', [[:Telescope buffers<cr>]] },
        -- history
        { '<c-h>', [[:Telescope oldfiles<cr>]] },
        { '<c-s>', [[<cmd>lua require'telescope'.extensions.luasnip.luasnip{}<CR>]], mode = { 'n', 'i' } },
        { '<leader>gb', [[<cmd>lua require'telescope.builtin'.git_branches()<CR>]], mode = { 'n', 'i' } },
    }
}
