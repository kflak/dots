return {
    'luisiacc/gruvbox-baby',
    enabled = false,
    config = function()
        vim.cmd([[colorscheme gruvbox-baby]])
    end
}
