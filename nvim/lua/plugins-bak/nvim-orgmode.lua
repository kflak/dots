return {
    'nvim-orgmode/orgmode',
    config = function()
        require'orgmode'.setup({
            org_agenda_files = {'~/org/**'},
            org_default_notes_file = '~/org/notes.org',
            org_todo_keywords = {'TODO(t)', 'WAITING(w)', '|', 'DONE(d)'},
            -- org_custom_exports = ['p'] = {
            --     ['export using pdf.sh'] = function(exporter)
            --         return exporter()
            -- end
            -- }
            notifications = {
                enabled = true,
                cron_enabled = false
            }
        })
        vim.opt.conceallevel = 2
        vim.opt.concealcursor = 'nc'
        vim.opt.formatoptions  = 'jcql'
    end
}
