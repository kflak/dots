return {
    'L3MON4D3/LuaSnip',
    keys = {
        {'<leader>s', ':lua require("luasnip.loaders").edit_snippet_files()<CR>'},
        {'<leader>r', ':lua require("luasnip.loaders.from_lua").load({paths = "~/.config/nvim/lua/snippets"})<CR>'},
    },
    config = function()
        require'luasnip'.setup{
            history = false
        }
    end,
}
