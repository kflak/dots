return {
    'neovim/nvim-lspconfig',
    config = function()
        local lsp = require'lspconfig'

        lsp.marksman.setup{ }
        lsp.texlab.setup{}
        lsp.bashls.setup{ }
        lsp.vimls.setup{ }
        lsp.jsonls.setup { }
        lsp.yamlls.setup { }
        lsp.clojure_lsp.setup { }
        -- Taken care of by rustaceanvim instead
        -- lsp.rust_analyzer.setup {
        --     -- Server-specific settings. See `:help lspconfig-setup`
        --     settings = {
        --         ['rust-analyzer'] = {},
        --     },
        -- }
        lsp.lua_ls.setup{
            -- cmd = { "/usr/bin/lua-language-server", "-E", "/usr/bin/lua-language-server/main.lua"},
            settings = {
                Lua = {
                    runtime = {
                        version = 'LuaJIT',
                        path = vim.split(package.path, ';'),
                    },
                    diagnostics = {
                        globals = {
                            "vim",
                            "use",
                            "luasnip",
                            "has_words_before"
                        },
                        workspace = {
                            library = vim.api.nvim_get_runtime_file("", true),
                        },
                        telemetry = {
                            enable = false,
                        },
                    }
                }
            }
        }
        lsp.clangd.setup{
        }
        -- ccls is set up separately in ranjithshegde/nvim-ccls
        -- lsp.ccls.setup{}
        lsp.yamlls.setup{ }
        lsp.pylsp.setup{}
        vim.lsp.set_log_level("off")
        -- lsp.set_log_level("debug") -- uncommented this when needed...
    end,
    dependencies = {
        'https://git.sr.ht/~p00f/clangd_extensions.nvim/'
    }
}
