return {
    'lervag/vimtex',
    -- enabled = false,
    init = function()
        vim.g.vimtex_view_method = 'zathura'
        vim.g.vimtex_imaps_enabled = 0
        vim.g.vimtex_compiler_latexmk = {
            options = {
                "-shell-escape",
            }
        }
        vim.g.vimtex_compiler_progname = 'nvr'
        vim.g.vimtex_quickfix_autoclose_after_keystrokes = 1
    end
}
