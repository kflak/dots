return {
    'kana/vim-textobj-entire',
    dependencies = {
        'kana/vim-textobj-user'
    },
    keys = {
        {'in', '<Plug>(textobj-entire-i)', mode = {'o'}},
        -- {'in', '<Plug>(textobj-entire-i)', mode = {'n', 'o', 'v'}},
    }
}
