return {
    'Pocco81/auto-save.nvim',
    config = function()
        require'auto-save'.setup{
            enabled = true,
            execution_message = {
                message = "",
                -- dim = 0.1,
                -- cleaning_interval = 100
            },
            condition = function(buf)
                local fn = vim.fn -- built in neovim functions
                local utils = require("auto-save.utils.data") -- auto-save utilities for handling data
                if fn.getbufvar(buf, "&modifiable") == 1 and utils.not_in(fn.getbufvar(buf, "&filetype"), {"oil", "man"}) then -- conditions to be asserted
                    return true -- met condition(s), can save
                end
                return false -- can't save
            end,
        }
    end
}

