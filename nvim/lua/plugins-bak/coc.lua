return {
    enabled = false,
    'neoclide/coc.nvim',
    branch = 'release',
    ft = "lilypond"
}
