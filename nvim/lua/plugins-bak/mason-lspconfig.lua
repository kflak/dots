return {
    'williamboman/mason-lspconfig.nvim',
    config = function()
        require('mason-lspconfig').setup({
            ensure_installed = {
                'bashls',
                'lua_ls',
                'clangd',
                'marksman',
            }
        })
    end
}
