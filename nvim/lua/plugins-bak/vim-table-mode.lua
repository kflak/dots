return {
    'dhruvasagar/vim-table-mode',
    init = function()
        vim.g.table_mode_corner = '|'
        vim.g.table_mode_auto_align = 1
    end
}
