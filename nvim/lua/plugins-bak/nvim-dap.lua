return {
    'mfussenegger/nvim-dap',
    config = function()
        local dap = require('dap')
        dap.adapters.cppdbg = {
            id = 'cppdbg',
            type = 'executable',
            command = 'OpenDebugAD7',
            -- command = '/home/kf/build/extension/debugAdapters/bin/OpenDebugAD7',
        }
        dap.configurations.cpp = {
            {
                name = "Launch file",
                type = "cppdbg",
                request = "launch",
                program = function()
                    return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/bin/', 'file')
                end,
                cwd = '${workspaceFolder}',
                stopAtEntry = true,
            },
            {
                name = 'Attach to gdbserver :1234',
                type = 'cppdbg',
                request = 'launch',
                MIMode = 'gdb',
                miDebuggerServerAddress = 'localhost:1234',
                miDebuggerPath = '/usr/bin/gdb',
                cwd = '${workspaceFolder}',
                program = function()
                    return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file')
                end,
            }
        }
    end,
    dependencies = {
        'rcarriga/nvim-dap-ui',
        {
            'theHamsta/nvim-dap-virtual-text',
            config = true,
            -- config = function()
            --     require'nvim-dap-virtual-text'.setup()
            -- end
        },
        'nvim-telescope/telescope-dap.nvim',
    },
    keys = {
        {'<leader>db', [[:lua require'dap'.toggle_breakpoint()<cr>]]},
        {'<leader>dc', [[:lua require'dap'.continue()<cr>]]},
        {'<leader>dr', [[:lua require'dap'.repl.toggle()<cr>]]},
        {'<leader>dl', [[:lua require'dap'.run_last()<cr>]]},
        {'<F10>', [[:lua require'dap'.step_over()<cr>]]},
        {'<leader>di', [[:lua require'dap'.step_into()<cr>]]},
        {'<leader>df', [[:lua require'dap'.step_out()<cr>]]},
    }
}
