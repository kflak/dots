return {
    'timtro/glslView-nvim',
    ft = 'glsl',
    dependencies = { 'tikhomirov/vim-glsl' },
    keys = {
        {'F6', [[:GlslView<cr>]]}
    }
}
