return {
    "nvim-neorg/neorg",
    -- build = ":Neorg sync-parsers",
    dependencies = {
        -- "nvim-lua/plenary.nvim" ,
        -- "nvim-neorg/neorg-telescope",
        "luarocks.nvim"
    },
    ft = "norg",
    lazy = true,
    opts = {
        load = {
            ["core.defaults"] = {}, -- Loads default behaviour
            ["core.concealer"] = {}, -- Adds pretty icons to your documents
            ["core.dirman"] = { -- Manages Neorg workspaces
                config = {
                    workspaces = {
                        notes = "~/norg",
                        programming = "~/norg/programming",
                    },
                    default_workspace = "notes",
                },
            },
            ["core.completion"] = {
                config = {
                    engine = "nvim-cmp",
                }
            },
            ["core.ui.calendar"] = {},
            ["core.export"] = { },
            ["core.export.markdown"] = { },
            ["core.qol.todo_items"] = {
                config = {
                    create_todo_parents = true
                }
            },
            -- ["core.integrations.telescope"] = { },
            -- ["core.presenter"] = {
            --     config = {
            --         zen_mode = "zen-mode",
            --     }
            -- },
            ["core.keybinds"] = {
                config = {
                    hook = function(keybinds)
                        -- Keybinds to make moving sections up and down easily
                        keybinds.map("norg", "n", "]s", "<cmd>Neorg keybind norg core.integrations.treesitter.next.heading<cr>")
                        keybinds.map("norg", "n", "[s", "<cmd>Neorg keybind norg core.integrations.treesitter.previous.heading<cr>")
                        -- Keybinds for the journal module
                        keybinds.map("norg", "n", "<leader>njc", '<cmd>Neorg journal custom<cr>')
                        keybinds.map("norg", "n", "<leader>njt", '<cmd>Neorg journal today<cr>')
                        keybinds.map("norg", "n", "<leader>njf", '<cmd>Neorg journal tomorrow<cr>')
                        keybinds.map("norg", "n", "<leader>njy", '<cmd>Neorg journal yesterday<cr>')

                        -- Keybinds for the calendar module
                        keybinds.map("norg", "n", "<leader>nc", '<cmd>:lua neorg.modules.get_module("core.ui.calendar").select_date({})<cr>')
                        keybinds.map("norg", "n", "<leader>ni", '<cmd>:Neorg inject-metadata<cr>')
                    end,
                }
            }
        },
    }
}
