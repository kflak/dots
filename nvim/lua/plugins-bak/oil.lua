return {
    'stevearc/oil.nvim',
    enabled = false,
    config = function()
        require("oil").setup({
            delete_to_trash = true,
            keymaps = {
                ["y"] = "actions.copy_entry_path"
            }
        })
    end,
    keys = {
        {'-', [[:Oil<cr>]]}
    },
    lazy = false,
    ft = "oil"
}
