return {
    'ranjithshegde/ccls.nvim',
    config = function()
        -- TODO: create separate module for this
        local filetypes = { "c", "cpp", "objc", "objcpp", "opencl" }
        local server_config = {
            filetypes = filetypes,
            init_options = {
                cache = {
                    directory = vim.env.XDG_CACHE_HOME .. "/ccls/",
                },
            },
        }
        require("ccls").setup {
            lsp = {
                lspconfig = server_config,
                disable_capabilities = {
                    completionProvider = true,
                    documentFormattingProvider = true,
                    documentRangeFormattingProvider = true,
                    documentHighlightProvider = true,
                    documentSymbolProvider = true,
                    workspaceSymbolProvider = true,
                    renameProvider = true,
                    hoverProvider = true,
                    codeActionProvider = true,
                },
                disable_diagnostics = true,
                disable_signature = true,
            },
        }
    end,
    enabled = false
}
