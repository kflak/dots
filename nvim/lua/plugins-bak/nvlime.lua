return {
    'monkoose/nvlime',
    enabled = true,
    config = function()
        vim.g.nvlime_config = {
            cmp = { enabled = true },
            autodoc = {
                enabled = false,
                max_level = 5,
                max_lines = 50
            },
        }
    end,
    dependencies = {
        -- {'PaterJason/nvim-treesitter-sexp', config = true},
        'monkoose/parsley',
        'guns/vim-sexp',
        'tpope/vim-sexp-mappings-for-regular-people',
        -- 'gpanders/nvim-parinfer'
    }
}
