return {
    "catppuccin/nvim",
    enabled = true,
    name = "catppuccin",
    priority = 1000,
    opts = {
        highlight_overrides = {
            mocha = function(colors)
                return {
                    Comment = { fg = colors.flamingo },
                }
            end,
        },
        integrations = {
            cmp = true,
            gitsigns = true,
            nvimtree = true,
            treesitter = true,
            notify = true,
            mini = true,
        },
    },
    config = function()
        require("catppuccin").setup ({
            vim.cmd([[colorscheme catppuccin]])
        })
    end
}
