return {
    'jakemason/ouroboros',
    init = function()
        vim.api.nvim_create_user_command('A', 'Ouroboros', {})
    end,
    dependencies = {
        'nvim-lua/plenary.nvim',
    },
    ft = {'c', 'cpp'},
    -- enabled = false
}
