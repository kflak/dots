return {
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup({
            ignore = '^$'
        })
        local ft = require('Comment.ft')
        ft({'openscad', 'faust'}, {'//%s', '/*%s*/'})
        ft.csound = ';%s'
    end,
}
