local M = {}
local map = vim.keymap.set

function M.mappings()
    M.div()
    M.movements()
end

function M.movements()
    map({'v', 'n', 'o'}, 'H', '^')
    map({'v', 'n', 'o'}, 'L', '$')
    -- map('n', 'j', 'gj')
    -- map('n', 'k', 'gk')
end

function M.div()
    -- exit terminal mode
    map('t', '<Esc>', '<C-\\><C-n>')
end

function M.lisp()
    map('i',[[']], [[']])
    map('i','`', '`')
end

return M
