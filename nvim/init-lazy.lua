require 'mappings'.mappings()
require 'settings'.settings()
require 'autocommands'

vim.cmd([[source ~/.config/nvim/abbreviations.vimrc]])
vim.cmd([[autocmd FileType lilypond,scheme,lisp,clojure inoremap <buffer> ' ']])

local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath,
    })
end

vim.opt.rtp:prepend(lazypath)

require'lazy'.setup("plugins", {
    dev = { path = "~/build" },
    change_detection = {
        -- automatically check for config file changes and reload the ui
        enabled = false,
        notify = false, -- get a notification when changes are found
    },
})
