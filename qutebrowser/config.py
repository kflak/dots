# unbinds some standard qutebrowser bindings. M-x will handle missing
# functions
c.bindings.default = {}

# reload config
config.bind('<Ctrl-x><Ctrl-l>', 'config-source')

# generic navigation
config.bind('<Ctrl-E>', 'edit-text', mode='insert')
config.bind('<Alt-,>', 'scroll-to-perc 0')
config.bind('<Alt-.>', 'scroll-to-perc')
config.bind('<Ctrl-n>', 'scroll down')
config.bind('<Ctrl-p>', 'scroll up')
config.bind('<Alt-[>', 'back')
config.bind('B', 'back')
config.bind('<Alt-]>', 'forward')
config.bind('F', 'forward')
config.bind('<Ctrl-x><Ctrl-f>', 'cmd-set-text -s :open -t')
config.bind('G', 'cmd-set-text -s :open -t')
config.bind('<Ctrl-u><Ctrl-x><Ctrl-f>', 'cmd-set-text -s :open')
config.bind('g', 'cmd-set-text -s :open')
config.bind('<Ctrl-l>', 'cmd-set-text -s :open')
config.bind('<Alt-,>', 'scroll-to-perc 0')
config.bind('<Alt-.>', 'scroll-to-perc')

config.bind('<Escape>', 'mode-leave', mode='insert')
config.bind('<Ctrl-g>', 'mode-leave', mode='insert')
config.bind('<Ctrl-m>', 'mode-enter insert')

# caret mode
config.bind('<Ctrl-space>', 'mode-enter caret')
config.bind('<Ctrl-space>', 'selection-toggle', mode='caret')
config.bind('<Return>', 'yank selection', mode='caret')
config.bind('<Esc>', 'mode-leave', mode='caret')
config.bind('<Ctrl-g>', 'mode-leave', mode='caret')
config.bind('<Alt-x>', 'cmd-set-text :', mode='caret')
config.bind('<Ctrl-a>', 'move-to-start-of-line', mode='caret')
config.bind('<Ctrl-e>', 'move-to-end-of-line', mode='caret')
config.bind('<Ctrl-b>', 'move-to-prev-char', mode='caret')
config.bind('<Ctrl-f>', 'move-to-next-char', mode='caret')
config.bind('<Ctrl-p>', 'move-to-prev-line', mode='caret')
config.bind('<Ctrl-n>', 'move-to-next-line', mode='caret')
config.bind('<Alt-f>', 'move-to-next-word', mode='caret')
config.bind('<Alt-b>', 'move-to-prev-word', mode='caret')


# close qutebrowser
config.bind('<Ctrl-x><Ctrl-c>', 'quit') 

# tab management
config.bind('<Ctrl-x>0', 'tab-close')
config.bind('q', 'tab-close')
config.bind('<Ctrl-x>1', 'tab-only')
config.bind('<Alt-a>', 'tab-prev')
config.bind('<Ctrl-c><Ctrl-p>', 'tab-prev')
config.bind('<Alt-e>', 'tab-next')
config.bind('<Ctrl-c><Ctrl-n>', 'tab-next')
config.bind('r', 'reload')
config.bind('c','yank')

# searching
config.bind('<Ctrl-s>', 'cmd-set-text /', mode='normal')
config.bind('<Ctrl-r>', 'cmd-set-text ?', mode='normal')
config.bind('<Ctrl-s>', 'search-next', mode='command')
config.bind('<Ctrl-r>', 'search-prev', mode='command')

# zooming
config.bind('<Ctrl-=>', 'zoom-in')
config.bind('<Ctrl-->', 'zoom-out')

# command mode
config.bind('<Alt-x>', 'cmd-set-text :')
config.bind('<Up>', 'command-history-prev', mode='command')
config.bind('<Alt-[>', 'command-history-prev', mode='command')
config.bind('<Down>', 'command-history-next', mode='command')
config.bind('<Alt-]>', 'command-history-next', mode='command')
config.bind('<Escape>', 'mode-leave', mode='command')
config.bind('<Ctrl-g>', 'mode-leave', mode='command')
config.bind('<Return>', 'command-accept', mode='command')
config.bind('<Ctrl-m>', 'command-accept', mode='command')
config.bind('<Shift-Tab>', 'completion-item-focus prev', mode='command')
config.bind('<Ctrl-Shift-i>', 'completion-item-focus prev', mode='command')
config.bind('<Tab>', 'completion-item-focus next', mode='command')
config.bind('<Ctrl-n>', 'completion-item-focus next', mode='command')
config.bind('<Ctrl-p>', 'completion-item-focus prev', mode='command')


# promt mode
config.bind('<Up>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-p>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Down>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-n>', 'prompt-item-focus next', mode='prompt')
config.bind('<Escape>', 'mode-leave', mode='prompt')
config.bind('<Ctrl-g>', 'mode-leave', mode='prompt')
config.bind('<Return>', 'prompt-accept', mode='prompt')
config.bind('<Ctrl-m>', 'prompt-accept', mode='prompt')
config.bind('<Shift-Tab>', 'prompt-item-focus prev', mode='prompt')
config.bind('<Ctrl-Shift-i>', 'prompt-item-focus next', mode='prompt')
config.bind('<Tab>', 'prompt-item-focus next', mode='prompt')
config.bind('<Ctrl-i>', 'prompt-item-focus next', mode='prompt')
config.bind('n', 'prompt-accept no', mode='prompt')
config.bind('y', 'prompt-accept yes', mode='prompt')

# hinting
config.bind('<Ctrl-u>f', 'hint --rapid links tab-bg')
config.bind('f', 'hint')
config.bind('i', 'hint inputs')
config.bind('cf', 'hint links yank-primary') 
config.bind('<Escape>', 'mode-leave', mode='hint')
config.bind('<Ctrl-g>', 'mode-leave', mode='hint')
config.bind('<Return>', 'follow-hint', mode='hint')
config.bind('<Ctrl-m>', 'follow-hint', mode='hint')

#yesno
config.bind('y','prompt-accept yes',mode='yesno')
config.bind('n','prompt-accept no',mode='yesno')
config.bind('Y','prompt-accept --save yes',mode='yesno')
config.bind('N','prompt-accept --save no',mode='yesno')
config.bind('<Alt-w>','prompt-yank',mode='yesno')
config.bind('<Alt-u><Alt-w>','prompt-yank --sel',mode='yesno')
config.bind('<Escape>','mode-leave',mode='yesno')
config.bind('<Ctrl-g>','mode-leave',mode='yesno')
config.bind('<Return>','prompt-accept',mode='yesno')

#user scripts
# config.bind('<Ctrl-R>', 'config-cycle content.user_stylesheets ~/.config/qutebrowser/gruvbox-all-sites.css ""')
config.bind('<Ctrl-c><Ctrl-m', 'spawn mpv {url}')
config.bind('<Alt-p>', 'spawn --userscript qute-pass')
config.load_autoconfig()

#settings
c.zoom.default = 150
c.fonts.default_size = "14pt"
c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}", "aw": "https://wiki.archlinux.org/?search={}", "wi": "https://wikipedia.org/wiki/{}", "gl": "https://glosbe.com/en/nb/{}"}
c.content.autoplay = False
c.auto_save.session = True

## External editor. Launch by hitting Ctrl-e when in insert mode
c.editor.command = ["emacsclient", "-c", "+{line}:{column}", "{file}"]
