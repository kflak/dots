[
    {
        "name": "topbar",
        "layer": "top", // Waybar at top layer
        "ipc": true,
        "position": "top", // Waybar at the top of your screen
        "height": 28, // Waybar height
        // "modules-left": ["hyprland/window", "hyprland/submap"],
        "modules-left": ["sway/window", "sway/mode"],
        "modules-right": ["network", "jack", "pulseaudio", "disk", "cpu","temperature", "battery", "clock"],

        //"hyprland/submap": {
        //     "format": " {} ",
        //},
        "sway/window": {
            "format": " {title} ",
            "max-length": 80,
        },
        "clock": {
            "timezone": "Europe/Tallinn",
            "format": "{:%a %d/%m/%Y %H:%M}",
            "on-click": "evolution"
        },
        "cpu": {
            "format": "CPU {usage}%",
            "tooltip": false
        },
        "network": {
            "format-wifi": "{essid} {ipaddr} {signalStrength}% ",
            "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
            "format-disconnected": "Disconnected ",
            "on-click": "if pgrep iwgtk; then pkill iwgtk; else iwgtk; fi"
        },
        "battery": {
            "states": {
                "good": 95,
                "warning": 30,
                "critical": 15
            },
            "format": " BAT {capacity}%",
        },
        "battery#bat2": {
            "bat": "BAT2"
        },
        "temperature": {
            // "thermal-zone": 2,
            // "hwmon-path": "/sys/class/hwmon/hwmon2/temp1_input",
            "critical-threshold": 80,
            // "format-critical": "{ temperatureC}°C {icon}",
            "format": "{temperatureC}°C ",
            "format-icons": ["", "", ""]
        },
        "backlight": {
            // "device": "acpi_video1",
            "format": "{percent}% {icon}",
            "format-icons": ["", ""]
        },
        "jack": {
            "format": " DSP {load}% / {latency} ms / {bufsize} ",
            "format-xrun": "{xruns} xruns",
            "format-disconnected": "DSP off",
            "realtime": true,
            "on-click": "qjackctl"
        },
        "disk": {
            "format": " {used}/{total} ",
            "path": "/"
        },
        "custom/vpn": {
            "exec": "vpn_module.sh",
            "on-click": "vpn_module.sh --toggle-connection",
          //  "click-right":  "vpn_module.sh --location-menu",
          //  "click-middle": "vpn_module.sh --ip_address",
            "interval": 5,
            "format": "vpn "
        },
        "custom/alsa": {
            "exec": "amixer -D hw:PCH get Master | sed -nre 's/.*\\[off\\].*/  muted/p; s/.*\\[(.*%)\\].*/  \\1/p'",
            "on-click": "amixer -D hw:PCH set Master toggle; pkill -x -RTMIN+11 waybar",
            "on-scroll-up": "amixer -D hw:PCH set Master 1+; pkill -x -RTMIN+11 waybar",
            "on-scroll-down": "amixer -D hw:PCH set Master 1-; pkill -x -RTMIN+11 waybar",
            "signal": 11,
            "interval": 10,
            "tooltip": false
        },
        "pulseaudio": {
            "format": " {volume}% {icon} ",
            "format-bluetooth": " {volume}% {icon} ",
            "format-muted": "",
            "format-icons": {
                "headphone": "",
                "hands-free": "",
                "headset": "",
                "phone": "",
                "portable": "",
                "car": "",
                "default": ["", ""]
            },
            "scroll-step": 1,
            "on-click": "pavucontrol",
            "ignored-sinks": ["Easy Effects Sink"]
        },
        "wireplumber": {
            "format": " {volume}% {icon} ",
            "format-muted": "",
            "on-click": "pavucontrol",
            "format-icons": ["", "", ""]
        }
    },
    {
        "name": "bottombar",
        "ipc": true,
        "layer": "top", // Waybar at top layer
        "position": "bottom", // Waybar at the bottom of your screen
        //"modules-left": ["hyprland/workspaces", "nordvpn"],
        "modules-left": ["sway/workspaces",],
        "modules-right": ["custom/weather", "custom/display_layout", "mpris", "tray"],
        "custom/weather": {
            "format": "{} °",
            "tooltip": true,
            "interval": 30,
            "exec": "wttrbar --location Pärnu",
            "return-type": "json"
        },
        "tray": {
            "icon-size": 25,
            "spacing": 10,
            "show-passive-items": true
        },
        "hyprland/workspaces": {
            "sort-by-number": true
        },
        "mpris": {
            "format": "{player_icon} {dynamic}",
            "format-paused": " {status_icon} <i>{dynamic}</i>  ",
            "player-icons": {
                "default": "▶",
                "mpv": "🎵"
            },
            "status-icons": {
                "paused": "⏸"
            },
            "on-click": "playerctl play-pause",
            // "ignored-players": ["firefox"]
        },
        "custom/display_layout": {
            "format": "Displays ",
            "on-click": "if pgrep nwg-displays; then pkill nwg-displays; else nwg-displays -n 10; fi"
        },
        "custom/nordvpn": {
            "interval": 5,
            "format": "VPN{}",
            "exec": "go-waybar-nordvpn exec",
            "on-click": "go-waybar-nordvpn toggle",
            "return-type": "json",
        },
    }
]

// vim:ft=json:conceallevel=0
