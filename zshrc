# -*-Shell-script-*-

autoload -U promptinit; promptinit
# requires pure promt, available from AUR
# prompt pure

autoload -Uz compinit

typeset -i updated_at=$(date +'%j' -r ~/.zcompdump 2>/dev/null || stat -f '%Sm' -t '%j' ~/.zcompdump 2>/dev/null)
if [ $(date +'%j') != $updated_at ]; then
    compinit -i
else
    compinit -C -i
fi

zmodload -i zsh/complist

HISTFILE=$HOME/.zsh_history
HISTSIZE=100000
SAVEHIST=$HISTSIZE
DIRSTACKSIZE=10

setopt hist_ignore_all_dups # remove older duplicate entries from history
setopt hist_reduce_blanks # remove superfluous blanks from history items
setopt inc_append_history # save history entries as soon as they are entered
setopt share_history # share history between different instances of the shell
setopt auto_cd
setopt autopushd pushdminus pushdsilent pushdtohome pushdignoredups
setopt auto_list # automatically list choices on ambiguous completion
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match
setopt extendedglob
setopt menucomplete
zstyle ':completion:*' menu select # select completions with arrow keys
zstyle ':completion:*' group-name '' # group results by category
zstyle ':completion:::::' completer _expand _complete _ignored _approximate # enable approximate matches for completion
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' #resolve upper and lower cases

bindkey '^R' history-incremental-pattern-search-backward


export CALCURSE_CALDAV_PASSWORD=$(pass show nextcloud05.webo.cloud/kennethflak@protonmail.com) 

export VISUAL="emacsclient"
export EDITOR="emacsclient"
export DIFFPROG="emacsclient"

# export TERM=xterm-256color
export TERM=foot
# export TERM=xterm-kitty
export XDG_CACHE_HOME=~/.cache
export TZ='Europe/Tallinn'

export QT_QPA_PLATFORMTHEME=qt5ct

if pgrep sway > /dev/null
then  
    export MOZ_ENABLE_WAYLAND=1
    export QT_QPA_PLATFORM=wayland
    export XDG_SESSION_TYPE=wayland
    export XDG_CURRENT_DESKTOP=sway
    export QT_WAYLAND_DISABLE_WINDOWDECORATION=1 
    export QT_SCALE_FACTOR=1
fi 

# in order for alacritty to scale fonts correctly across different 
# monitors
export WINIT_HIDPI_FACTOR=1.0

export LANG=en_US.UTF-8
export LC_PAPER=nb_NO.UTF-8
export PAPERSIZE=a4
export OPENWEATHERMAP_PLACE="Pärnu,Estonia"

export PATH="$PATH:/home/kf/.gem/ruby/3.0.0/bin:/home/kf/.gem/ruby/2.7.0/bin:/home/kf/.gem/ruby/2.6.0/bin:/home/kf/.gem/ruby/2.5.0/bin:/home/kf/bin:/usr/lib/python2.7:/usr/lib/python3.7:/home/kf/go/bin:/home/kf/of:/home/kf/bin/fuzzy-yogurt:/home/kf/bin/rofi-jack:/home/kf/.local/bin:/home/kf/.cargo/bin:/opt/resolve/:/root/.local/ahre/gem/ruby/3.0.0/bin:/home/kf/quicklisp"


export SC_JACK_DEFAULT_INPUTS="system"
export SC_JACK_DEFAULT_OUTPUTS="system"

export PG_OF_PATH={{pg_of_path}}
export OF_ROOT=$PG_OF_PATH

export BROWSER='/usr/bin/firefox'
# export BROWSER='/usr/bin/qutebrowser' 
# export WINEARCH=win32
# export WINEPREFIX=~/win32 
export MATES_DIR=~/.contacts/woelkli/contacts 

export FZF_DEFAULT_OPTS='--layout=reverse'

export GITLAB_TOKEN='$HOME/glab-token'
export LYEDITOR=emacs
source ~/.aliases.sh

eval "$(zoxide init zsh)"

source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

function mcd() {
    mkdir -p -- "$1" && cd -- "$1"
}

# Automatically expand all aliases
preexec_functions=()

function expand_aliases {
    input_command=$1
    expanded_command=$2
    if [ $input_command != $expanded_command ]; then
        print -nP $PROMPT
        echo $expanded_command
    fi
}

preexec_functions+=expand_aliases

function colo() {
    local colors='~/.config/alacritty/base16-alacritty/colors/'
    local config='~/.config/alacritty/alacritty.yml'
    local cmd="exa $colors"
    local pcmd="bat -f $colors{}"
    sk --ansi -c $cmd --preview $pcmd \
        --bind "enter:execute(echo 'new alacritty colors: {}' && sed -i '1,/^$/cimport:\n- $colors{}\n' $config)+abort"
    }
ranger_cd() {
    temp_file="$(mktemp -t "ranger_cd.XXXXXXXXXX")"
    ranger --choosedir="$temp_file" -- "${@:-$PWD}"
    if chosen_dir="$(cat -- "$temp_file")" && [ -n "$chosen_dir" ] && [ "$chosen_dir" != "$PWD" ]; then
        cd -- "$chosen_dir"
    fi
    rm -f -- "$temp_file"
}

cc() {
    echo "$@" | bc -l
}

# salkinmada's amazing functions for previewing media files and/or getting
# information about them
function sma() {
    local cmd='fd . -tf | rg -i ".*[.](wav|mp3|flac|aif(f)?)"'
    local pcmd="mpv --no-audio-display {}"
    sk -m -c $cmd --preview $pcmd --preview-window=right:0%
}

function smas() {
    local cmd='fd . -tf | rg -i ".*[.](wav|mp3|flac|aif(f)?)"'
    local pcmd="soxi {}"
    sk -m -c $cmd --preview $pcmd --preview-window=down:40%:wrap \
        --bind 'ctrl-e:execute(mpv --fs=no {})'
    }

function smv() {
    local cmd='fd . -tf | rg -i ".*[.](mov|mp4|avi|mkv)"'
    local pcmd="mediainfo {}"
    sk -m -c $cmd --preview $pcmd --preview-window=down:60%:wrap \
        --bind 'ctrl-e:execute(mpv --fs=no {})' \
        --bind 'ctrl-f:execute(mpv --fs=yes {})'
    }

# ripgrep all files 
rga-fzf() {
	RG_PREFIX="rga --files-with-matches"
	local file
	file="$(
		FZF_DEFAULT_COMMAND="$RG_PREFIX '$1'" \
			fzf --sort --preview="[[ ! -z {} ]] && rga --pretty --context 5 {q} {}" \
				--phony -q "$1" \
				--bind "change:reload:$RG_PREFIX {q}" \
				--preview-window="70%:wrap"
	)" &&
	echo "opening $file" &&
	xdg-open "$file"
}

mirror-display () {
  swaymsg -t get_outputs | jq -r '.[]."name" | select(. != null)' | fzf --preview="{swaymsg -t get_outputs | jq -r '.[].current_mode'}" --bind "enter:execute-silent:wl-mirror {} &"
}

function ya() {
	local tmp="$(mktemp -t "yazi-cwd.XXXXX")"
	yazi "$@" --cwd-file="$tmp"
	if cwd="$(cat -- "$tmp")" && [ -n "$cwd" ] && [ "$cwd" != "$PWD" ]; then
		cd -- "$cwd"
	fi
	rm -f -- "$tmp"
}

eval "$(starship init zsh)"

source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

ZSH_AUTOSUGGEST_STRATEGY=(history completion)
bindkey '^F' forward-word
bindkey "^b" backward-word
bindkey '^a' autosuggest-accept
bindkey '^e' autosuggest-execute
bindkey '^ ' autosuggest-toggle
