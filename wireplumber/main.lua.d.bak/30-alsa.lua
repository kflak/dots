alsa_monitor.rules = {
    matches = {
        {
            -- { "node.name", "matches", "alsa_input.*" },
            { "alsa.driver_name", "equals", "snd_hda_intel" },
        },
    },
    apply_properties = {
        ["api.alsa.period-size"] = 512,
    }
}
