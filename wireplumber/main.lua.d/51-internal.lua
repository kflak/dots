rule = {
    matches = {
        {
            {"node.name", "equals", "alsa_card.pci-0000_00_1f.2"}
        }
    },
    apply_properties = {
        -- ["device.disabled"] = true
        -- ["api.alsa.period-size"]   = 2,
        -- ["api.alsa.period-num"]    = 2048,
        -- ["node.nick"] = "ALC257",
    }
}

table.insert(alsa_monitor.rules, rule)
