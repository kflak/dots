(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(*cltl2-home* "/home/kf/.config/common-lisp/cltl2-docs/")
 '(LaTeX-amsmath-label "eq:")
 '(LaTeX-electric-left-right-brace t)
 '(LilyPond-pdf-command "zathura" t)
 '(Man-notify-method 'aggressive t)
 '(TeX-auto-save t)
 '(TeX-electric-math '("$" . "$"))
 '(TeX-electric-sub-and-superscript t)
 '(TeX-parse-self t)
 '(TeX-source-correlate-start-server t)
 '(TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view)))
 '(TeX-view-program-selection '((output-pdf "PDF Tools")))
 '(auctex-latexmk-inherit-TeX-PDF-mode t)
 '(battery-mode-line-format " [BAT:%p%%] ")
 '(bookmark-save-flag 1)
 '(browse-url-handlers
   '(("file:///home/kf/quicklisp/HyperSpec" . eww-browse-url)
     ("." . browse-url-default-browser)))
 '(cape-dabbrev-check-other-buffers 'same-mode-buffers)
 '(codeium/metadata/api_key "c1381606-9df6-46a6-8d94-7e20726a1a44")
 '(column-number-mode t)
 '(common-lisp-hyperspec-root "file:///home/kf/quicklisp/HyperSpec/" t)
 '(compilation-ask-about-save nil)
 '(completion-category-overrides '((file (styles partial-completion))))
 '(completion-cycle-threshold 3)
 '(completion-preview-idle-delay 0.1)
 '(completion-styles '(orderless basic))
 '(completions-detailed t)
 '(corfu-auto t)
 '(corfu-auto-prefix 2)
 '(corfu-cycle t)
 '(custom-enabled-themes '(ef-autumn))
 '(custom-safe-themes
   '("00d7122017db83578ef6fba39c131efdcb59910f0fac0defbe726da8072a0729"
     "8d820dc0b48dae30f0a76629665fc81f3d7dd150c008f48d8533646ba80188de"
     "9fba87dbc0f14d5650006893ed53088be71f16d57b749394d9c485ef2326e85f"
     "622df781877694637a1ee23272d147bd395dfbfbc58842bec2683d4f49a0ae38"
     "6755a9491a6287586f3c2e57e0fc7411afab62e10a1401fc88147ec06317eafe"
     "d0dc7861b33d68caa92287d39cf8e8d9bc3764ec9c76bdb8072e87d90546c8a3"
     "2dd4951e967990396142ec54d376cced3f135810b2b69920e77103e0bcedfba9"
     "3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa"
     "7776ba149258df15039b1f0aba4b180d95069b2589bc7d6570a833f05fdf7b6d"
     default))
 '(delete-by-moving-to-trash t)
 '(dired-auto-revert-buffer t)
 '(dired-dwim-target t)
 '(diredp-hide-details-initially-flag nil)
 '(diredp-image-preview-in-tooltip 300)
 '(display-time-24hr-format t)
 '(display-time-day-and-date t)
 '(eat-term-scrollback-size nil)
 '(ediff-window-setup-function 'ediff-setup-windows-plain t)
 '(eglot-autoshutdown t)
 '(eldoc-echo-area-use-multiline-p t)
 '(eshell-modules-list
   '(eshell-alias eshell-banner eshell-basic eshell-cmpl eshell-dirs
                  eshell-smart eshell-extpipe eshell-glob eshell-hist
                  eshell-ls eshell-pred eshell-prompt eshell-script
                  eshell-term eshell-unix))
 '(eshell-scroll-to-bottom-on-input 'this t)
 '(fast-but-imprecise-scrolling t)
 '(fido-mode nil)
 '(focus-follows-mouse t)
 '(global-auto-revert-non-file-buffers t)
 '(hippie-expand-try-functions-list
   '(yas-hippie-try-expand try-complete-file-name-partially
                           try-complete-file-name
                           try-expand-all-abbrevs try-expand-list
                           try-expand-line try-expand-dabbrev
                           try-expand-dabbrev-all-buffers
                           try-expand-dabbrev-from-kill
                           try-complete-lisp-symbol-partially
                           try-complete-lisp-symbol))
 '(ibuffer-movement-cycle nil)
 '(ibuffer-old-time 24)
 '(inferior-lisp-program "sbcl")
 '(kill-do-not-save-duplicates t)
 '(load-prefer-newer t t)
 '(markdown-enable-html t)
 '(markdown-enable-math t)
 '(menu-bar-mode nil)
 '(mode-line-compact 'long)
 '(mouse-autoselect-window t)
 '(olivetti-body-width 90)
 '(olivetti-minimum-body-width 40)
 '(org-hide-emphasis-markers t)
 '(org-latex-compiler "lualatex")
 '(org-link-descriptive t)
 '(org-mouse-1-follows-link t)
 '(org-noter-notes-search-path '("~/org"))
 '(org-return-follows-link t)
 '(package-archive-priorities
   '(("gnu" . 99) ("nongnu" . 80) ("stable" . 70) ("melpa" . 0)))
 '(package-selected-packages nil)
 '(package-vc-selected-packages
   '((codeium :url "https://github.com/Exafunction/codeium.el")))
 '(pdf-misc-print-program-executable "/usr/bin/lpr")
 '(pdf-view-continuous nil)
 '(reftex-plug-into-AUCTeX t t)
 '(ring-bell-function 'ignore)
 '(safe-local-variable-values
   '((buffer-read-only . 1)
     (projectile-project-compilation-cmd . "make -j8")))
 '(scheme-program-name "guile")
 '(sclang-auto-scroll-post-buffer t)
 '(sclang-eval-line-forward nil)
 '(sclang-show-workspace-on-startup nil)
 '(scroll-bar-mode nil)
 '(scroll-conservatively 101)
 '(scroll-margin 0)
 '(scroll-preserve-screen-position t)
 '(send-mail-function 'smtpmail-send-it)
 '(shr-max-width 80)
 '(sly-command-switch-to-existing-lisp ''always)
 '(sly-enable-evaluate-in-emacs t)
 '(split-width-threshold 100)
 '(switch-to-buffer-in-dedicated-window 'pop)
 '(switch-to-buffer-obey-display-actions t)
 '(tab-always-indent 'complete)
 '(tab-bar-mode nil)
 '(text-mode-ispell-word-completion nil)
 '(tool-bar-mode nil)
 '(treesit-auto-langs '(cpp css html bash))
 '(use-short-answers t)
 '(user-mail-address "kennethflak@protonmail.com")
 '(xref-show-definitions-function 'xref-show-definitions-completing-read)
 '(yas-triggers-in-field t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Source Code Pro" :foundry "ADBO" :slant normal :weight regular :height 158 :width normal))))
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:foreground "royal blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))))
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))
