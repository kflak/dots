;;; package --- Summary: My personal emacs configuration
;;; Commentary:
;;; This is my very own emaaaacs!!!
;;; Code:
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (and custom-file
           (file-exists-p custom-file))
   (load custom-file nil :nomessage))
;; TODO:
;; - (x) port all of the crafted-emacs stuff over to my own config.
;; - ( ) modularize the configuration.
;; (load "~/crafted-emacs/modules/crafted-init-config.el")

;; (defun crafted-emacs-load-modules (modules)
;;   "Initialize crafted-emacs modules.

;;      MODULES is a list of module names without the -packages or
;;      -config suffixes.  Note that any user-provided packages should be
;;      added to `package-selected-packages' before invoking this
;;      function."
;;   (dolist (m modules)
;;     (require (intern (format "crafted-%s-packages" m)) nil :noerror))
;;   (package-install-selected-packages :noconfirm)
;;   (dolist (m modules)
;;     (require (intern (format "crafted-%s-config" m)) nil :noerror)))

;; (customize-set-variable 'package-selected-packages '(ef-themes magit))

(use-package ef-themes
  :ensure t)
(use-package magit
  :ensure t)
;; (crafted-emacs-load-modules '(ui))

;; UI

(use-package breadcrumb
  :ensure t
  :config
  (breadcrumb-mode))

(use-package helpful
  :bind
  (("C-h f" . helpful-callable)
   ("C-h v" . helpful-variable)
   ("C-h k" . helpful-key)
   ("C-h x" . helpful-command)
   ("C-c C-d" . helpful-at-point)
   ("C-h F" . helpful-function)))

;; add visual pulse when changing focus, like beacon but built-in
;; from from https://karthinks.com/software/batteries-included-with-emacs/
(defun pulse-line (&rest _)
  "Pulse the current line."
  (pulse-momentary-highlight-one-line (point)))

(dolist (command '(scroll-up-command
                   scroll-down-command
                   recenter-top-bottom
                   other-window))
  (advice-add command :after #'pulse-line))

;; Writing
(use-package markdown-mode
  :ensure t)
(use-package pandoc-mode
  :ensure t)
(use-package auctex
  :ensure t)
(use-package auctex-latexmk
  :ensure t)

(electric-pair-mode 1)
(show-paren-mode 1)

(with-eval-after-load 'latex
  (customize-set-variable 'TeX-auto-save t)
  (customize-set-variable 'TeX-parse-self t)
  (setq-default TeX-master nil)

  ;; compile to pdf
  (tex-pdf-mode)

  ;; correlate the source and the output
  (TeX-source-correlate-mode)

  ;; set a correct indentation in a few additional environments
  (add-to-list 'LaTeX-indent-environment-list '("lstlisting" current-indentation))
  (add-to-list 'LaTeX-indent-environment-list '("tikzcd" LaTeX-indent-tabular))
  (add-to-list 'LaTeX-indent-environment-list '("tikzpicture" current-indentation))

  ;; add a few macros and environment as verbatim
  (add-to-list 'LaTeX-verbatim-environments "lstlisting")
  (add-to-list 'LaTeX-verbatim-environments "Verbatim")
  (add-to-list 'LaTeX-verbatim-macros-with-braces "lstinline")
  (add-to-list 'LaTeX-verbatim-macros-with-delims "lstinline")

  ;; electric pairs in auctex
  (customize-set-variable 'TeX-electric-sub-and-superscript t)
  (customize-set-variable 'LaTeX-electric-left-right-brace t)
  (customize-set-variable 'TeX-electric-math (cons "$" "$"))

  ;; open all buffers with the math mode and auto-fill mode
  (add-hook 'LaTeX-mode-hook #'auto-fill-mode)
  (add-hook 'LaTeX-mode-hook #'LaTeX-math-mode)

  ;; add support for references
  (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
  (customize-set-variable 'reftex-plug-into-AUCTeX t)

  ;; to have the buffer refresh after compilation
  (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)

  (customize-set-variable 'TeX-view-program-selection '((output-pdf "PDF Tools")))
  (customize-set-variable 'TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view)))
  (customize-set-variable 'TeX-source-correlate-start-server t)

  (when (require 'auctex-latexmk nil 'noerror)
    (with-eval-after-load 'auctex-latexmk
      (auctex-latexmk-setup)
      (customize-set-variable 'auctex-latexmk-inherit-TeX-PDF-mode t))

    (defun crafted-writing-tex-make-latexmk-default-command ()
      "Set `TeX-command-default' to \"LatexMk\"."
      (setq TeX-command-default "LatexMk"))
    (add-hook 'TeX-mode-hook #'crafted-writing-tex-make-latexmk-default-command)))

;;; Markdown
(when (fboundp 'markdown-mode)
  ;; because the markdown-command variable may not be loaded (yet),
  ;; check manually for the other markdown processors.  If it is
  ;; loaded, the others are superfluous but `or' fails fast, so they
  ;; are not checked if `markdown-command' is set and the command is
  ;; indeed found.
  (unless (or (and (boundp 'markdown-command)
                   (executable-find markdown-command))
              (executable-find "markdown")
              (executable-find "pandoc"))
    (message "No markdown processor found, preview may not possible."))

  (with-eval-after-load 'markdown-mode
    (customize-set-variable 'markdown-enable-math t)
    (customize-set-variable 'markdown-enable-html t)
    (add-hook 'markdown-mode-hook #'conditionally-turn-on-pandoc)))

;;; PDF support when using pdf-tools
(when (locate-library "pdf-tools")
  ;; load pdf-tools when going into doc-view-mode
  (defun crafted-writing-load-pdf-tools ()
    "Attempts to require pdf-tools, but for attaching to hooks."
    (require 'pdf-tools nil :noerror))
  (add-hook 'doc-view-mode-hook #'crafted-writing-load-pdf-tools)

  ;; when pdf-tools is loaded, apply settings.
  (with-eval-after-load 'pdf-tools
    (setq-default pdf-view-display-size 'fit-width)))

;; defaults
;; y-or-n instead of yes-or-no
(customize-set-variable 'use-short-answers t)

;; Don't ask about opening large files
(customize-set-variable 'large-file-warning-threshold nil)

;; no more splash on startup
(setq inhibit-startup-message t
      inhibit-startup-echo-area-message t)

;; just kill the process..
(setq kill-buffer-query-functions
  (remq 'process-kill-buffer-query-function
         kill-buffer-query-functions))

;;; Buffers

;; Revert Dired and other buffers
(customize-set-variable 'global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

;; Make dired do something intelligent when two directories are shown
;; in separate dired buffers.  Makes copying or moving files between
;; directories easier.  The value `t' means to guess the default
;; target directory.
(customize-set-variable 'dired-dwim-target t)

;; dired plus
(load-file "~/.config/emacs/dired+.el")
(customize-set-variable 'diredp-image-preview-in-tooltip 300)
(customize-set-variable 'diredp-hide-details-initially-flag nil)

;; automatically update dired buffers on revisiting their directory
(customize-set-variable 'dired-auto-revert-buffer t)
;; pop up dedicated buffers in a different window.
(customize-set-variable 'switch-to-buffer-in-dedicated-window 'pop)
;; treat manual buffer switching (C-x b for example) the same as
;; programmatic buffer switching.
(customize-set-variable 'switch-to-buffer-obey-display-actions t)

;; prefer the more full-featured built-in ibuffer for managing
;; buffers.
(keymap-global-set "<remap> <list-buffers>" #'ibuffer-list-buffers)
;; turn off forward and backward movement cycling
(customize-set-variable 'ibuffer-movement-cycle nil)
;; the number of hours before a buffer is considered "old" by
;; ibuffer.
(customize-set-variable 'ibuffer-old-time 24)

(fido-vertical-mode 1)
(customize-set-variable 'tab-always-indent 'complete)
(customize-set-variable 'completion-cycle-threshold 3)
(customize-set-variable 'completion-category-overrides
                        '((file (styles . (partial-completion)))))
(customize-set-variable 'completions-detailed t)

;; use completion system instead of popup window
(customize-set-variable 'xref-show-definitions-function
                        #'xref-show-definitions-completing-read)

(delete-selection-mode)
;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Do not save duplicates in kill-ring
(customize-set-variable 'kill-do-not-save-duplicates t)

;; Better support for files with long lines
(setq-default bidi-paragraph-direction 'left-to-right)
(setq-default bidi-inhibit-bpa t)
(global-so-long-mode 1)

;; define a key to define the word at point.
(keymap-set global-map "M-#" #'dictionary-lookup-definition)

;; Show dictionary definition on the left
(add-to-list 'display-buffer-alist
             '("^\\*Dictionary\\*"
               (display-buffer-in-side-window)
               (side . left)
               (window-width . 70)))

;; turn on spell checking, if available.
(with-eval-after-load 'ispell
  (when (executable-find ispell-program-name)
    (add-hook 'text-mode-hook #'flyspell-mode)
    (add-hook 'prog-mode-hook #'flyspell-prog-mode)))
;; Turn on recentf mode
(add-hook 'after-init-hook #'recentf-mode)

;; Enable savehist-mode for command history
(savehist-mode 1)

;; save the bookmarks file every time a bookmark is made or deleted
;; rather than waiting for Emacs to be killed.  Useful especially when
;; Emacs is a long running process.
(customize-set-variable 'bookmark-save-flag 1)
;; Make scrolling less stuttered
(setq auto-window-vscroll nil)
(customize-set-variable 'fast-but-imprecise-scrolling t)
(customize-set-variable 'scroll-conservatively 101)
(customize-set-variable 'scroll-margin 0)
(customize-set-variable 'scroll-preserve-screen-position t)

;; open man pages in their own window, and switch to that window to
;; facilitate reading and closing the man page.
(customize-set-variable 'Man-notify-method 'aggressive)

;; keep the Ediff control panel in the same frame
(customize-set-variable 'ediff-window-setup-function
                        'ediff-setup-windows-plain)

;; Window configuration for special windows.
(add-to-list 'display-buffer-alist
             '("\\*Help\\*"
               (display-buffer-reuse-window display-buffer-pop-up-window)))

(add-to-list 'display-buffer-alist
             '("\\*Completions\\*"
               (display-buffer-reuse-window display-buffer-pop-up-window)
               (inhibit-same-window . t)
               (window-height . 10)))
;;; Miscellaneous

;; Load source (.el) or the compiled (.elc or .eln) file whichever is
;; newest
(customize-set-variable 'load-prefer-newer t)

;; Make shebang (#!) file executable when saved
(add-hook 'after-save-hook
          #'executable-make-buffer-file-executable-if-script-p)

(winner-mode 1)
;; completion, replacing the crafted-emacs Completion module

(use-package cape
  :ensure t)
(use-package consult
  :ensure t
  :hook (completion-list-mode . consult-preview-at-point-mode)
  :init

  ;; Tweak the register preview for `consult-register-load',
  ;; `consult-register-store' and the built-in commands.  This improves the
  ;; register formatting, adds thin separator lines, register sorting and hides
  ;; the window mode line.
  (advice-add #'register-preview :override #'consult-register-window)
  (setq register-preview-delay 0.5)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref))

(use-package embark
  :ensure t

  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :init

  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)

  ;; Show the Embark target at point via Eldoc. You may adjust the
  ;; Eldoc strategy, if you want to see the documentation from
  ;; multiple providers. Beware that using this can be a little
  ;; jarring since the message shown in the minibuffer can be more
  ;; than one line, causing the modeline to move up and down:

  ;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
  ;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)

  :config

  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

;; Consult users will also want the embark-consult package.
(use-package embark-consult
  :ensure t ; only need to install it, embark loads it after consult if found
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

(use-package marginalia
  :ensure t
  :init
  (marginalia-mode)
  :custom
  (marginalia-annotators
   '(marginalia-annotators-heavy
     marginalia-annotators-light
     nil))
  )

(use-package cape
  ;; Bind prefix keymap providing all Cape commands under a mnemonic key.
  ;; Press C-c p ? to for help.
  ;; :bind ("C-c p" . cape-prefix-map) ;; Alternative key: M-<tab>, M-p, M-+
  ;; Alternatively bind Cape commands individually.
  ;; :bind (("C-c p d" . cape-dabbrev)
  ;;        ("C-c p h" . cape-history)
  ;;        ("C-c p f" . cape-file)
  ;;        ...)
  :init
  ;; Add to the global default value of `completion-at-point-functions' which is
  ;; used by `completion-at-point'.  The order of the functions matters, the
  ;; first function returning a result wins.  Note that the list of buffer-local
  ;; completion functions takes precedence over the global list.
  (add-hook 'completion-at-point-functions #'cape-dabbrev)
  (add-hook 'completion-at-point-functions #'cape-file)
  (add-hook 'completion-at-point-functions #'cape-elisp-block)
  ;; (add-hook 'completion-at-point-functions #'cape-history)
  ;; ...
  )
(use-package orderless
  :ensure t)
(use-package vertico
  :ensure t
  :init
  (vertico-mode)
  :custom
  (vertico-cycle t)
  :config
  (require 'vertico-directory)
  (vertico-mode 1)
  (fido-mode -1)
  (fido-vertical-mode -1)
  (icomplete-mode -1)
  (icomplete-vertical-mode -1))

(customize-set-variable 'completion-preview-idle-delay 0.1)

(customize-set-variable 'compilation-ask-about-save nil)
;; (remove-hook 'c-mode-common-hook #'tree-sitter-hl-mode)

(use-package glsl-mode
  :ensure t)

(use-package magit-lfs
  :ensure t)

(setq treesit-language-source-alist
      '((bash "https://github.com/tree-sitter/tree-sitter-bash")
        (cmake "https://github.com/uyha/tree-sitter-cmake")
        (css "https://github.com/tree-sitter/tree-sitter-css")
        (elisp "https://github.com/Wilfred/tree-sitter-elisp")
        (html "https://github.com/tree-sitter/tree-sitter-html")
        (javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
        (json "https://github.com/tree-sitter/tree-sitter-json")
        (make "https://github.com/alemuller/tree-sitter-make")
        (markdown "https://github.com/ikatyang/tree-sitter-markdown")
        (python "https://github.com/tree-sitter/tree-sitter-python")
        (toml "https://github.com/tree-sitter/tree-sitter-toml")
        (yaml "https://github.com/ikatyang/tree-sitter-yaml")
        (glsl "https://github.com/tree-sitter-grammars/tree-sitter-glsl")))

(setq major-mode-remap-alist
      '((yaml-mode . yaml-ts-mode)
        (bash-mode . bash-ts-mode)
        (json-mode . json-ts-mode)
        (css-mode . css-ts-mode)
        (c++-mode . c++-ts-mode)
        (python-mode . python-ts-mode)
        (glsl-mode . glsl-ts-mode)))

(use-package eglot
  :ensure t
  :hook (c++-ts-mode . eglot-ensure)
  :bind (("C-c k" . eglot-code-actions)
         ("C-c r" . eglot-rename))
  :custom
  (eglot-ignored-server-capabilities
   '(:documentHighlightProvider
     :documentFormattingProvider
     :documentRangeFormattingProvider
     :documentOnTypeFormattingProvider
     :colorProvider
     :foldingRangeProvider)))

;; (use-package lsp-mode
;;   :init
;;   ;; set prefix for lsp-command-keymap (few alternatives - "C-l", "C-c l")
;;   (setq lsp-keymap-prefix "C-l")
;;   :hook (;; replace XXX-mode with concrete major-mode(e. g. python-mode)
;;          (c++-ts-mode . lsp)
;;          ;; if you want which-key integration
;;          (lsp-mode . lsp-enable-which-key-integration))
;;   :commands lsp)
;; (use-package lsp-ui
;;   :ensure t
;;   :commands lsp-ui-mode)
(defun clang-format-buffer-smart ()
  "Reformat buffer if .clang-format exists in the projectile root."
  (when (f-exists? (expand-file-name ".clang-format" (projectile-project-root)))
    (clang-format-buffer)))

(defun clang-format-buffer-smart-on-save ()
  "Add auto-save hook for clang-format-buffer-smart."
  (add-hook 'before-save-hook #'clang-format-buffer-smart nil t))
(add-hook 'c-ts-base-mode-hook #'clang-format-buffer-smart-on-save)

(add-to-list 'safe-local-variable-values
             '(projectile-project-compilation-cmd . "make -j8"))

(remove-hook 'prog-mode-hook #'flyspell-mode)
(add-hook 'prog-mode-hook #'rainbow-delimiters-mode)
;; Search, reverting crafted-emacs' preference for consult-line
(global-set-key (kbd "C-s") #'isearch-forward)

;; Use ibuffer instead of built-in buffer list:
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Don't ask before reverting
(global-auto-revert-mode 1)

;; Follow mouse focus. Don't want to have to click in the window...
(customize-set-variable 'mouse-autoselect-window t)

;; Get rid of bars I don't want
(customize-set-variable 'tool-bar-mode nil)
(customize-set-variable 'scroll-bar-mode nil)
(customize-set-variable 'menu-bar-mode nil)

;; Make native compilation silent and prune its cache.
;; From here: https://github.com/protesilaos/dotfiles/blob/master/emacs/.emacs.d/prot-emacs.org
(when (native-comp-available-p)
  (setq native-comp-async-report-warnings-errors 'silent) ; Emacs 28 with native compilation
  (setq native-compile-prune-cache t)) ; Emacs 29

;; visual-fill-column
(use-package visual-fill-column
  :ensure t)

;; Highlight todos
(global-hl-todo-mode +1)

;; Aggressive indent
(add-hook 'program-mode-hook #'aggressive-indent-mode)

;; Limit dabbrev. from here: https://emacs.stackexchange.com/questions/81813/can-cape-dabbrev-be-made-to-skip-buffers-of-certain-modes
;; (customize-set-variable 'cape-dabbrev-check-other-buffers
;;       (lambda () (match-buffers
;;           (lambda (buffer)
;;             (not
;;              (buffer-match-p
;;               (cons 'derived-mode 'pdf-view-mode)
;;               buffer)))
;;           (buffer-list))))
(customize-set-variable 'cape-dabbrev-check-other-buffers 'same-mode-buffers)


;; lisp setup

(setq inferior-lisp-program "/usr/bin/sbcl")

(customize-set-variable 'common-lisp-hyperspec-root
                        (concat "file://" (expand-file-name "~/quicklisp/HyperSpec/")))
;; yasnippet setup
(use-package yasnippet
  :ensure t
  :bind (("C-c C-<return>" . #'yas-expand))
  :config
  (add-to-list 'yas-snippet-dirs "~/dots/emacs/snippets")
  (yas-global-mode 1))

(use-package helm-c-yasnippet
  :ensure t)


;; Get rid of corfu error looking for non-existent alternate dictionary
;; From here: https://github.com/minad/corfu/discussions/457
(customize-set-variable 'text-mode-ispell-word-completion nil)
;; Make sure corfu ditches pop-up after no match.
;; (setq corfu-quit-no-match t)
;; (keymap-unset corfu-map "RET") ; use tab instead...

(column-number-mode +1)

(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'nxml-mode-hook  (lambda () (flycheck-mode -1)))
(global-set-key (kbd "C-c C-p") #'flycheck-previous-error)
(global-set-key (kbd "C-c C-n") #'flycheck-next-error)


(use-package djvu
  :ensure t)

(defun buffer-kill-path ()
  "copy buffer's full path to kill ring"
  (interactive)
  (kill-new (buffer-file-name)))

(use-package perspective
  :ensure t
  :bind
  ;; global overrides
  (("C-x C-b" . persp-ibuffer)
   ;; ("C-x b" . persp-switch-to-buffer*)
   ;; asks for confirmation
   ;; ("C-x k" . persp-kill-buffer*)
   )
  
  :custom
  (persp-mode-prefix-key (kbd "C-x C-x"))
  (persp-modestring-short t)

  :init
  (persp-mode))

(use-package persp-projectile
  :ensure t)

(use-package pdf-tools
  :if (package-installed-p 'pdf-tools)
  :mode (("\\.pdf\\'" . pdf-view-mode))
  :bind (:map pdf-view-mode-map
              ("M-w" . pdf-view-kill-ring-save))
  :hook ((pdf-view-mode . pdf-sync-minor-mode)
         (pdf-view-mode . pdf-isearch-minor-mode)
         (pdf-view-mode . pdf-annot-minor-mode))
  :config
  (setopt pdf-view-display-size 'fit-width
          pdf-annot-activate-created-annotations t
          pdf-view-use-scaling nil)
  (require 'saveplace-pdf-view)
  (save-place-mode 1))

(global-diff-hl-mode)

;; Projectile and helm setup
(use-package projectile
  :ensure t
  :diminish projectile-mode
  :init
  (projectile-mode +1)
  :bind-keymap
  ("C-c p" . projectile-command-map))
(add-hook 'project-find-functions #'project-projectile)

;; (use-package helm-projectile
;;   :init
;;   (helm-projectile-on))

(use-package helm
  :ensure t
  :diminish helm-mode)
(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)
;; (global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-x C-d") #'helm-browse-project)
;; (global-set-key (kbd "M-x") #'helm-M-x)
(helm-mode -1)

;; completion extras
(setq completion-styles '(basic substring partial-completion flex))

;; pulseaudio control
(pulseaudio-control-default-keybindings)

;; ;; smart-mode-line
;; (use-package smart-mode-line
;;   :ensure t
;;   :config
;;   (sml/setup))
(display-time-mode 1)
(display-battery-mode 1)
;; (use-package diminish
;;   :ensure t
;;   :config
;;   (diminish editorconfig-mode)
;;   (diminish flycheck-mode)
;;   (diminish olivetti-mode)
;;   (diminish org-indent-mode)
;;   (diminish buffer-face-mode)
;;   )
(use-package nerd-icons)
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1))

;; even better: The nuclear option. Get rid of all mode information in the modeline.
;; From here: https://stackoverflow.com/questions/71261843/how-can-i-hide-all-major-minor-modes-from-the-mode-line-without-a-package
(setq-default mode-line-format (delq 'mode-line-modes mode-line-format))
;; ;; mood-line
;; (use-package mood-line

;;   ;; Enable mood-line
;;   :config
;;   (mood-line-mode)

;;   ;; Use pretty Fira Code-compatible glyphs
;;   :custom
;;   (mood-line-glyph-alist mood-line-glyphs-fira-code))

;; SuperCollider stuff

(add-to-list 'load-path "/home/kf/.local/share/SuperCollider/downloaded-quarks/scel/el")
(require 'sclang)
(defun kf/sclang-lock ()
  "Locking sclang and setting the space bar to evaluate the current defun."
  (interactive)
  (setq buffer-read-only t)
  (keymap-local-set "<SPC>" #'sclang-eval-region-or-line)
  (message "Locked sclang"))

(defun kf/sclang-unlock ()
  "Unlocking sclang and resetting the space bar."
  (interactive)
  (setq buffer-read-only nil)
  (keymap-local-unset "<SPC>")
  (message "Unlocked sclang"))

(defun kf/sclang-set-lock ()
  "Set local buffer's lock state."
  (interactive)
  (setq-local sclang-locked-p nil))

(add-hook 'sclang-mode-hook #'kf/sclang-set-lock 0 t)

;; From this: https://github.com/supercollider/scel/issues/35
(defun kf/sclang-nodetree ()
  "Open scsynth nodeTree."
  (interactive)
  (sclang-eval-string "s.plotTree;"))

(defun kf/sclang-meter ()
  "Open scsynth meter."
  (interactive)
  (sclang-eval-string "s.meter;"))

(defun kf/sclang-scope ()
  "Open scsynth scope."
  (interactive)
  (sclang-eval-string "s.scope;"))

(defun kf/sclang-freqscope ()
  "Open scsynth freqscope."
  (interactive)
  (sclang-eval-string "FreqScope.new;"))

(defun kf/sclang-japa ()
  "Open Japa. Linux only. Requires japa and the linuxutils quark:
https://github.com/madskjeldgaard/linuxutils-quark."
  (interactive)
  (sclang-eval-string "Japa.new"))

(defun kf/sclang-record ()
  "Toggle recording. The file name is constructed by the git hash and a
timestamp, and puts into a directory called rec in the current process directory.
If the directory does not exist, it will be created."
  (interactive)
  (sclang-eval-expression "if(s.isRecording){s.stopRecording}{s.record(\"rec\"+/+Git(thisProcess.nowExecutingPath.dirname).sha[0..5]++\"_\"++Date.localtime.stamp++\".wav\", numChannels: s.options.numOutputBusChannels)}"))

(defun kf/sclang-synthdefs ()
  "Go to my synthdefs."
  (interactive)
  (find-file "/home/kf/.local/share/SuperCollider/Extensions/KF/Classes/KFSynthDefs.sc"))

;; Thank you, undltd!!! https://xn--w5d.cc/2024/07/28/supercollider-emacs-highlight-eval.html
(defun kf/sclang-highlight-defun (&optional silent-p)
  (cl-multiple-value-bind (beg end) (sclang-point-in-defun-p)
    (and beg end (pulse-momentary-highlight-region beg end))))

(defun kf/sclang-highlight-region-or-line (&optional silent-p)
  (if (and transient-mark-mode mark-active)
      (pulse-momentary-highlight-region (region-beginning) (region-end))
    (pulse-momentary-highlight-one-line (point))))

(advice-add 'sclang-eval-defun :before #'kf/sclang-highlight-defun)
(advice-add 'sclang-eval-region-or-line :before #'kf/sclang-highlight-region-or-line)

(use-package sclang
  :config
  (setq sclang-show-workspace-on-startup nil)
  (add-to-list 'display-buffer-alist
               '("*SCLang:PostBuffer*" display-buffer-in-side-window
                 (side . right)
                 (window-width . 60)))
  :bind (:map sclang-mode-map
              ("<f5>" . #'kf/sclang-nodetree)
              ("<f6>" . #'kf/sclang-meter)
              ("<f7>" . #'kf/sclang-japa)
              ("<f8>" . #'kf/sclang-record)
              ("<f9>" . #'kf/sclang-synthdefs)))

;; csound

(use-package csound-mode
  :ensure t
  :custom
  (csound-skeleton-default-sr 48000)
  (csound-skeleton-default-ksmps 16)
  (csound-skeleton-default-options "-d -oadc -W -3")
  ;; (csound-skeleton-default-additional-header "#include \"PATH/TO/YOU/UDOs.udo\"")
  :mode (("\\.csd\\'" . csound-mode)
         ("\\.orc\\'" . csound-mode)
         ("\\.sco\\'" . csound-mode)
         ("\\.udo\\'" . csound-mode)))

;; lisp

(require 'eldoc)

;; aggressive-indent-mode for all lisp modes
(when (locate-library "aggressive-indent")
  (add-hook 'lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'clojure-mode-hook #'aggressive-indent-mode)
  (add-hook 'scheme-mode-hook #'aggressive-indent-mode))

;;; Emacs Lisp

(when (locate-library "package-lint-flymake")
  (add-hook 'emacs-lisp-mode-hook #'package-lint-flymake-setup))

(use-package package-lint
  :ensure t)
(use-package package-lint-flymake
  :ensure t)

;; common lisp
(use-package sly
  :ensure t
  :vc (:url "https://github.com/joaotavora/sly"
            :rev :newest))
;; (add-to-list 'load-path "/home/kf/build/sly")
;; (require 'sly-autoloads)

(use-package sly-repl-ansi-color
  :ensure t)
(use-package sly-asdf
  :ensure t)
(use-package sly-quicklisp
  :ensure t)

(with-eval-after-load 'sly
  (require 'sly-quicklisp "sly-quicklisp" :no-error)
  (require 'sly-repl-ansi-color "sly-repl-ansi-color" :no-error)
  (require 'sly-asdf "sly-asdf" :no-error)
  (add-hook 'sly-mode-hook
            (lambda ()
              (add-hook 'completion-at-point-functions #'yasnippet-capf nil t))))

(when (locate-library "sly")
  (add-hook 'lisp-mode-hook #'sly-editing-mode))

;; sly repl window
(add-to-list 'display-buffer-alist
             '("*sly-mrepl for sbcl*" display-buffer-in-side-window
               (side . right)
               (window-width . 60)))

;; clamps setup

(load "~/.config/emacs/browse-cltl2.el")
(setq *cltl2-home* "/home/kf/.config/common-lisp/cltl2-docs/")
(defun std-incudine-hush ()
  (interactive)
  (progn
    (sly-interactive-eval "(incudine:flush-pending)")
    (sly-interactive-eval "(dotimes (chan 16) (cm::sprout (cm::new cm::midi-control-change :time 0 :controller 123 :value 127 :channel chan)))")
    (sly-interactive-eval "(incudine::node-free-unprotected)")
;;;    (sly-interactive-eval "(scratch::node-free-all)")
    ))

(defun clamps-hush ()
  "call clamps::rts-hush in common lisp"
  (interactive)
  (sly-interactive-eval "(clamps::rts-hush)"))

(defun set-std-incudine-hush ()
  (interactive)
  (setq incudine-hush (symbol-function 'std-incudine-hush)))

(defun set-cm-incudine-hush ()
  (interactive)
  (setq incudine-hush (symbol-function 'cm-incudine-hush)))

(defun incudine-rt-start ()
  (interactive)
  (sly-interactive-eval "(incudine:rt-start)"))

(defun incudine-rt-stop ()
  (interactive)
  (sly-interactive-eval "(incudine:rt-stop)"))

(add-to-list 'load-path "~/quicklisp/local-projects/clamps/extra/elisp")
(require 'cm)

(customize-set-variable 'sly-enable-evaluate-in-emacs t)

(with-eval-after-load 'sly
  (add-hook
   'sly-mode-hook
   (lambda ()
     (define-key sly-mode-map (kbd "\C-c\C-dc") 'cm-lookup)
     (define-key sly-mode-map (kbd "C-.") 'clamps-hush)
     (define-key sly-mode-map (kbd "C-c C-.") 'incudine-rt-stop)
     (define-key sly-mode-map (kbd "C-c M-.") 'incudine-rt-start)
     (define-key sly-mode-map (kbd "C-c t") 'test-midi)
     (define-key sly-mode-map (kbd "C-c C-d l") 'cltl2-view-function-definition)
     (add-to-list 'completion-at-point-functions #'yasnippet-capf)))

  (load "clamps.el")
  (load "cm-dict.el")
  (load "clamps-dict.el")
 
  (add-hook
   'sly-mrepl-mode-hook
   (lambda ()
     (define-key sly-mrepl-mode-map (kbd "\C-c\C-dc") 'cm-lookup)
     (define-key sly-mrepl-mode-map (kbd "C-.") 'incudine-hush)
     (define-key sly-mrepl-mode-map (kbd "C-c C-.") 'incudine-rt-stop)
     (define-key sly-mrepl-mode-map (kbd "C-c M-.") 'incudine-rt-start)
     (define-key sly-mrepl-mode-map (kbd "C-c t") 'test-midi)
     (define-key sly-mrepl-mode-map (kbd "C-c C-d l") 'cltl2-view-function-definition)
     (add-to-list 'completion-at-point-functions #'yasnippet-capf))))

(customize-set-variable 'browse-url-handlers
 '(
   ("file:///home/kf/quicklisp/HyperSpec" . eww-browse-url)
   ("." . browse-url-default-browser)))

(use-package w3m)

(use-package which-key
  :config
  (which-key-mode))

;; flash evaluated lisp code
(define-advice eval-region (:before (start end &rest _) eval-region-pulse)
  (pulse-momentary-highlight-region start end))

;; Kill whole line if no region is defined
(use-package whole-line-or-region
  :ensure t
  :diminish whole-line-or-region-local-mode
  :config
  (whole-line-or-region-global-mode 1)
  )

;; (define-advice eval-last-sexp (:before (&rest _) eval-last-sexp-pulse)
;;   (save-excursion
;;     (forward-sexp -1)
;;     (let ((start (point)))
;;       (forward-sexp 1)
;;       (pulse-momentary-highlight-region start (point)))))

(setq backup-directory-alist '(("." . "~/.config/emacs/backup"))
      backup-by-copying t    ; Don't delink hardlinks
      version-control t      ; Use version numbers on backups
      delete-old-versions t  ; Automatically delete excess backups
      kept-new-versions 20   ; how many of the newest versions to keep
      kept-old-versions 5    ; and how many of the old
      )
;; Auto-save instead of polluting the environment with #####...
(auto-save-visited-mode 1)

(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

;; Also move the lock file to a temp-dir
(setq lock-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(defun kf/mu4e-compose-forward-html ()
    (interactive)
    (mu4e-action-capture-message (mu4e-message-at-point))
    (mu4e-compose-forward)
    (kf/mu4e-forward-html))

(use-package mu4e

  ;; TODO: Can't set hooks like this. Why not?
  ;; :hook ((mu4e-main-mode mu4e-view-mode mu4e-compose-mode) . variable-pitch-mode)
  ;; This, however, works. Not very DRY, though...
  :init
  (add-hook 'mu4e-main-mode-hook #'variable-pitch-mode)
  (add-hook 'mu4e-view-mode-hook #'variable-pitch-mode)
  (add-hook 'mu4e-compose-mode-hook #'variable-pitch-mode)
  (add-hook 'mu4e-compose-mode-hook (lambda ()  (use-hard-newlines -1)))
  ;; To avoid the editor getting sluggish when using completion-preview...
  (add-hook 'mu4e-compose-mode-hook (lambda () (setq-local completion-preview-idle-delay 0.1)))

  :config
  (setq mu4e-compose-format-flowed t)
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-change-filenames-when-moving t) ; needed for mbsync
  (setq mu4e-update-interval nil)       ; don't update automatically
  (setq mu4e-maildir "~/mail")
  (setq mu4e-attachment-dir "~/Downloads")
  (setq mu4e-split-view 'horizontal)
  (setq mu4e-headers-visible-lines 4)
  (setq mu4e-context-policy nil)
  (setq mu4e-compose-complete-only-after "2020-01-01")
  (setq smtpmail-auth-supported '(cram-md5 plain login xoauth2))
  (require 'secrets)
  ;; (setq auth-sources '((:source (:secrets "oama - kennethflak@gmail.com")
  ;;                               "~/.authinfo.gpg"
  ;;                       )))
  (setq auth-source "~/.authinfo.gpg")
  (setq mu4e-contexts
        (list
         (make-mu4e-context
          :name "Protonmail"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/Protonmail"(mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "kennethflak@protonmail.com")
                  (user-full-name . "Kenneth Flak")
                  (smtpmail-smtp-server . "127.0.0.1")
                  ;; (auth-sources . "~/.authinfo.gpg")
                  (smtpmail-stream-type . ssl)
                  (smtpmail-smtp-service . 1025)
                  (mu4e-drafts-folder . "/Protonmail/drafts")
                  (mu4e-sent-folder . "/Protonmailsent")
                  (mu4e-refile-folder . "/Protonmail/archive")
                  (mail-signature . t)
                  (mu4e-trash-folder . "/Protonmail/trash")))
         (make-mu4e-context
          :name "EMTA"
          :match-func
          (lambda (msg)
            (when msg
              (string-prefix-p "/emta"(mu4e-message-field msg :maildir))))
          :vars '((user-mail-address . "kenneth.flak@eamt.ee")
                  (user-full-name . "Kenneth Flak")
                  (smtpmail-smtp-server . "smtp.gmail.com")
                  (smtpmail-stream-type . starttls)
                  (smtpmail-smtp-service . 587)
                  (mu4e-drafts-folder . "/emta/[Gmail]/Drafts")
                  (mu4e-sent-folder . "/emta/[Gmail]/Sent Mail")
                  (mu4e-refile-folder . "/emta/[Gmail]/All Mail")
                  (mu4e-trash-folder . "/emta/[Gmail]/")))

         ))

  (setq mu4e-main-hide-personal-addresses t)

  (setq mu4e-compose-reply-ignore-address '("no-?reply" "kennethflak@protonmail.com"))

  (setq mu4e-compose-format-flowed t)
  (setq mu4e-maildir-shortcuts
        '((:maildir "/Protonmail/INBOX"     :key  ?p)
          (:maildir "/emta/INBOX" :key ?e)
          (:maildir "/emta/[Gmail]/Sent Mail" :key ?S)
          (:maildir "/lilypond"  :key  ?l)
          (:maildir "/aerc"     :key  ?a)
          (:maildir "/cm"     :key  ?c)
          (:maildir "/Protonmailsent"      :key  ?s)))
  (setq smtpmail-auth-supported '(cram-md5 plain login))
  ;; (require 'mu4e-speedbar)
  (setq mail-user-agent 'mu4e-user-agent)
  (set-variable 'read-mail-command 'mu4e)
  (add-to-list 'gnutls-trustfiles (expand-file-name "~/.config/protonmail/bridge/cert.pem"))

  (setq mm-text-html-renderer 'gnus-w3m)
  ;; I only want the mu4e modeline active when I'm in the mu4e buffer
  (setq mu4e-modeline-show-global nil)
  ;; from here: https://www.reddit.com/r/emacs/comments/18ddccr/forwarding_html_emails_in_mu4e/

  (add-to-list 'display-buffer-alist
               '("*mu4e-article*" display-buffer-in-direction
                 (direction . bottom)
                 (window-height . 100)))
  (define-key mu4e-view-mode-map ">" 'kf/mu4e-compose-forward-html)
  :bind (
         :map mu4e-main-mode-map ("C-<tab>" . nil)
         :map mu4e-thread-mode-map ("C-<tab>" . nil)
         ;; :map mu4e-compose-mode-map ("C-c C-a" . mail-add-attachment)
         ))

(use-package mastodon
  :ensure t
  :config
  (setq mastodon-instance-url "https://sonomu.club"
        mastodon-active-user "kf")
  (add-hook 'mastodon-mode-hook 'variable-pitch-mode))


;; Set up calendar.
(use-package calfw
  :config
  (setq calendar-week-start-day 1))
(use-package calfw-ical)

(defun kf/open-calendar ()
  "Open my nextcloud calendar."
  (interactive)
  (cfw:open-calendar-buffer
   :contents-sources
   (list
    (cfw:ical-create-source "Personal" "https://nextcloud05.webo.cloud/remote.php/dav/calendars/kennethflak@protonmail.com/personal/?export" "Purple")
    )))
;; Unfortunately it seems calfw can't create
;; any events, only read them... so enter khal:
(use-package khalel
  :custom
  (khalel-capture-key "e")
  (khalel-default-calendar "personal")
  (khalel-import-org-file (concat org-directory "/" "calendar.org"))
  (khalel-add-capture-template))


;; lilypond setup
(autoload 'LilyPond-mode "lilypond-mode")
(setq auto-mode-alist
      (cons '("\\.ly$" . LilyPond-mode) auto-mode-alist))
(setq auto-mode-alist
      (cons '("\\.lyi$" . LilyPond-mode) auto-mode-alist))
(customize-set-variable 'LilyPond-pdf-command "zathura")
(add-hook 'LilyPond-mode-hook (lambda () (turn-on-font-lock)))

;; python setup
(add-hook 'python-mode-hook
          (lambda ()
            (define-key python-mode-map "(" 'electric-pair)
            (define-key python-mode-map "{" 'electric-pair)))
;; snd setup
(load-file "~/dots/emacs/inf-snd.el")
(set-default 'auto-mode-alist
             (append '(("\\.rbs$" . snd-ruby-mode)
                       ("\\.cms$" . snd-scheme-mode))
                     auto-mode-alist))
(autoload 'run-snd-scheme    "inf-snd" "Start inferior Snd-Scheme process" t)
(setq inf-snd-scheme-program-name "snd -separate")
(add-to-list 'display-buffer-alist
             '("*Snd-Scheme*" display-buffer-in-side-window
               (side . right)
               (window-width . 60)))

;; start the server
;; (server-start)

;;; Dired-settings
(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (require 'dired-x))

(use-package dired-hide-dotfiles
  :ensure t
  :hook (dired-mode . dired-hide-dotfiles-mode)
  :bind (:map dired-mode-map
              ("h" . dired-hide-dotfiles-mode)))

(use-package all-the-icons-dired
  :ensure t
  :hook (dired-mode . all-the-icons-dired-mode))
;; Instead of opening a new buffer every time I open a dir or go up
;; one level
;; (use-package dired-single)

(use-package dired-open
  :ensure t
  :config
  (setq dired-open-extensions '(("mkv" . "mpv")
                                ("wav" . "mpv")
                                ("aiff" . "mpv")
                                ("mp4" . "mpv")
                                ("mov" . "mpv")
                                ("mp3" . "mpv")
                                ("webm" . "mpv")
                                ("odt" . "libreoffice")
                                ("ods" . "libreoffice")
                                ("docx" . "libreoffice")
                                ("xlsx" . "libreoffice"))))
;; (use-package dired-preview
;;   :config
;;   (setq dired-preview-delay 0.2)
;;   (setq dired-preview-max-size (expt 2 20))
;;   (setq dired-preview-ignored-extensions-regexp
;;         (concat "\\."
;;                 "\\(mkv\\|webm\\|mp4\\|mp3\\|ogg\\|m4a"
;;                 "\\|gz\\|zst\\|tar\\|xz\\|rar\\|zip"
;;                 "\\|iso\\|epub\\|pdf\\)"))

;;   ;; Enable `dired-preview-mode' in a given Dired buffer or do it
;;   ;; globally:
;;   (dired-preview-global-mode 0))

;; (use-package dired-rainbow
;;   :config
;;   (progn
;;     (dired-rainbow-define-chmod directory "#6lcb2eb" "d.*")
;;     (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
;;     (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
;;     (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
;;     (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
;;     (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
;;     (dired-rainbow-define media "#de751f" ("mp3" "mp4" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
;;     (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
;;     (dired-rainbow-define log "#c17d11" ("log"))
;;     (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
;;     (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
;;     (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
;;     (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
;;     (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
;;     (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
;;     (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
;;     (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
;;     (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
;;     (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
;;     (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*")
;;     ))

(use-package dired-subtree
  :ensure t)

;; paredit-mode settings
(autoload 'enable-paredit-mode "paredit"
  "Turn on pseudo-structural editing of Lisp code."
  t)
(add-hook 'emacs-lisp-mode-hook       'enable-paredit-mode)
(add-hook 'lisp-mode-hook             'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(add-hook 'scheme-mode-hook           'enable-paredit-mode)
(add-hook 'snd-scheme-mode-hook       'enable-paredit-mode)


;; Show line numbers in programming modes only
(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'lisp-mode-hook #'visual-wrap-prefix-mode)
(add-hook 'emacs-lisp-mode-hook #'visual-wrap-prefix-mode)
;; (remove-hook 'prog-mode-hook #'visual-wrap-prefix-mode)
;; enable pretty writing
(add-hook 'text-mode-hook #'olivetti-mode)
(add-hook 'Info-mode-hook #'olivetti-mode)
(add-hook 'mu4e-view-mode-hook #'olivetti-mode)
(add-hook 'sclang-mode-hook #'olivetti-mode)

;; enable hiding
(add-hook 'prog-mode-hook #'hs-minor-mode)

;; org mode
(global-set-key (kbd "C-c l") #'org-store-link)
(global-set-key (kbd "C-c a") #'org-agenda)
(global-set-key (kbd "C-c c") #'org-capture)

(use-package org-appear
  :ensure t
  :hook (org-mode . org-appear-mode))

;;; Return or left-click with mouse follows link
(customize-set-variable 'org-return-follows-link t)
(customize-set-variable 'org-mouse-1-follows-link t)

;;; Display links as the description provided
(customize-set-variable 'org-link-descriptive t)
;;; Disable auto-pairing of "<" in org-mode with electric-pair-mode

(defun crafted-org-enhance-electric-pair-inhibit-predicate ()
  "Disable auto-pairing of \"<\" in `org-mode' when using `electric-pair-mode'."
  (when (and electric-pair-mode (eql major-mode #'org-mode))
    (setq-local electric-pair-inhibit-predicate
                `(lambda (c)
                   (if (char-equal c ?<)
                       t
                     (,electric-pair-inhibit-predicate c))))))

;;; Electric Pair Mode
;; Add hook to both electric-pair-mode-hook and org-mode-hook
;; This ensures org-mode buffers don't behave weirdly,
;; no matter when electric-pair-mode is activated.
(add-hook 'electric-pair-mode-hook #'crafted-org-enhance-electric-pair-inhibit-predicate)
(add-hook 'org-mode-hook #'crafted-org-enhance-electric-pair-inhibit-predicate)

;;; Visually indent org-mode files to a given header level
(add-hook 'org-mode-hook #'org-indent-mode)

;;; Hide markup markers
(customize-set-variable 'org-hide-emphasis-markers t)

(setq org-directory "~/org")
(setq org-agenda-files (list org-directory))

(setq org-capture-templates
      `(("m" "Email Workflow")
        ("mf" "Follow Up" entry (file+headline "~/org/Mail.org" "Follow Up")
         "* TODO %a\n\n  %i")
        ("mr" "Read Later" entry (file+headline "~/org/Mail.org" "Read Later")
         "* TODO %a\n\n  %i")
        ("t" "Todo" entry (file+headline "~/org/todo.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("r" "Training" entry (file+datetree "~/org/training-diary.org" "Training Diary")
         "* %?")
        ("e" "EMTA" entry (file "~/org/emta.org")
         "* TODO \n%?")
        ("s" "Software")
        ("se" "Emacs" entry (file "~/org/emacs.org")
         "* \%?")
         ("sr" "Reaper" entry (file "~/org/Reaper.org")
         "* \%?")
         ("sc" "Clamps" entry (file "~/org/clamps.org")
         "* \%?")))

(setq org-todo-keywords
      '((sequence "TODO" "WAITING" "|" "DONE" "WON'T DO")))

(use-package org-pdftools
  :ensure t
  :hook (org-mode . org-pdftools-setup-link))

(add-hook 'text-mode-hook 'variable-pitch-mode)
;; From here: https://zzamboni.org/post/beautifying-org-mode-in-emacs/
(custom-theme-set-faces
 'user
 '(org-block ((t (:inherit fixed-pitch))))
 '(org-code ((t (:inherit (shadow fixed-pitch)))))
 '(org-document-info ((t (:foreground "dark orange"))))
 '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
 '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
 '(org-link ((t (:foreground "royal blue" :underline t))))
 '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-property-value ((t (:inherit fixed-pitch))) t)
 '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
 '(org-table ((t (:inherit fixed-pitch :foreground "#83a598"))))
 '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 0.8))))
 '(org-verbatim ((t (:inherit (shadow fixed-pitch))))))

;; compose html emails with org-mode, goddamit!
(use-package org-mime
  :ensure t)

;; From here: https://systemcrafters.net/emacs-tips/presentations-with-org-present/
(defun kf/org-present-start ()
  (setq visual-fill-column-width 110
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1)
  (visual-line-mode 1)
  ;; (tab-bar-mode -1)
  (setq-local face-remapping-alist '((default (:height 1.5) variable-pitch)
                                     (header-line (:height 4.0) variable-pitch)
                                     (org-document-title (:height 1.75) org-document-title)
                                     (org-code (:height 1.55) org-code)
                                     (org-verbatim (:height 1.55) org-verbatim)
                                     (org-block (:height 1.25) org-block)
                                     (org-block-begin-line (:height 0.7) org-block))))

(defun kf/org-present-end ()
  "Reset font customizations"
  (setq-local face-remapping-alist '((default variable-pitch default)))
  (visual-fill-column-mode 0)
  (visual-line-mode 0)
  ;; (tab-bar-mode 1)
  )

(defun kf/org-present-prepare-slide (buffer-name heading)

  ;; Show only top-level headlines
  (org-overview)

  ;; Unfold the current entry
  (org-show-entry)

  ;; Show only direct subheadings of the slide but don't expand them
  (org-show-children))

(use-package org-present
  :ensure t
  :config
  (remove-hook 'org-present-mode-hook
               (lambda ()
                 (if (org-present-mode)
                     'kf/org-present-start
                   'kf/org-present-start))))

(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-remove-inline-images)
                 (org-present-show-cursor)
                 (org-present-read-write)))
     (add-hook 'org-present-after-navigate-functions 'kf/org-present-prepare-slide)))

(use-package org-alert
  :ensure t)

(setq alert-default-style 'libnotify)
(setq org-alert-interval 300
      org-alert-notify-cutoff 10
      org-alert-notify-after-event-cutoff 10)

;; Adding sound to beamer presentation
;; From here: https://lists.gnu.org/archive/html/emacs-orgmode/2024-10/msg00019.html
;; example usage:
;; [[play:vlc --rate 3 --loop --fullscreen unfolding.mp4][unfolding.mp4]]
;; Unfortunately doesn't quite work...
(org-link-set-parameters "play" :export #'(lambda (path description backend 
                                                        info)
                                            (message "path=%s description=%s 
backend=%s info=%s" path description backend info)
                                            (cl-case backend
                                              ((beamer latex)
                                               (format "\\href{run:%s}{%s}\n" 
                                                       path description))
                                              (t
                                               (format "")))))

;; From here: https://github.com/GeneKao/orgmode-latex-templates
;; (setq org-latex-pdf-process
;; '("latexmk -pdflatex='pdflatex -interaction nonstopmode' -pdf -bibtex -f %f"))

(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))

(add-to-list 'org-latex-classes
             '("article"
               "\\documentclass[11pt,a4paper]{article}
\\usepackage[a4paper, margin=2.5cm]{geometry}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{fixltx2e}
\\usepackage{graphicx}
\\usepackage{longtable}
\\usepackage{float}
\\usepackage{wrapfig}
\\usepackage{rotating}
\\usepackage[normalem]{ulem}
\\usepackage{amsmath}
\\usepackage{textcomp}
\\usepackage{marvosym}
\\usepackage{wasysym}
\\usepackage{amssymb}
\\usepackage[colorlinks=true,urlcolor=magenta]{hyperref}
\\usepackage{mathpazo}
\\usepackage{color}
\\usepackage{parskip}
\\usepackage{enumerate}
\\definecolor{bg}{rgb}{0.95,0.95,0.95}
\\tolerance=1000
      [NO-DEFAULT-PACKAGES]
      [PACKAGES]
      [EXTRA]
\\linespread{1.1}
\\hypersetup{pdfborder=0 0 0}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")))
 
(global-set-key (kbd "C-:") 'avy-goto-char)
(global-set-key (kbd "C-;") 'avy-goto-char-2)

(global-set-key (kbd "M-o") 'other-window)

(customize-set-variable 'eldoc-echo-area-use-multiline-p t)

(use-package calibredb
  :config
  (setq calibredb-root-dir "~/calibre-library/")
  (setq calibredb-db-dir (expand-file-name "metadata.db" calibredb-root-dir))
  (setq calibredb-library-alist '(("~/calibre-library/")
                                  ("~/ebooks/"))))

;; Read ePub files
(use-package nov
  :init
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

(use-package surround
    :ensure t
    :bind-keymap ("C-c s" . surround-keymap))
(put 'downcase-region 'disabled nil)

(use-package undo-fu
  :config
  (global-unset-key (kbd "C-z"))
  (global-set-key (kbd "C-z")   'undo-fu-only-undo)
  (global-set-key (kbd "C-S-z") 'undo-fu-only-redo))

(use-package undo-fu-session
  :config
  (setq undo-fu-session-incompatible-files '("/COMMIT_EDITMSG\\'" "/git-rebase-todo\\'")))

(undo-fu-session-global-mode)

(global-set-key (kbd "C-c C-d") 'crux-duplicate-current-line-or-region)
(global-set-key (kbd "C-o") 'crux-smart-open-line)
(global-set-key (kbd "C-k") 'crux-smart-kill-line)


;; From here: https://www.masteringemacs.org/article/demystifying-emacs-window-manager
(setq switch-to-buffer-obey-display-actions t)

;; (desktop-save-mode 1)

(customize-set-variable 'split-width-threshold 100)

;; mpdel
(use-package mpdel
  :bind (("M-m" . 'mpdel-core-open-directories)))

;; emms
(use-package emms
  :ensure t
  :config
  (emms-all)
  (setq emms-player-list '(emms-player-mpv)
        emms-info-functions '(emms-info-native))
  (emms-mode-line-mode -1)
  (emms-playing-time-mode -1)
  (define-key emms-playlist-mode-map (kbd "<SPC>") #'emms-pause)
  (define-key emms-playlist-mode-map (kbd "C-b") #'emms-browser)
  (define-key emms-browser-mode-map (kbd "C-b") #'emms)
  (define-key emms-browser-mode-map (kbd "<SPC>") #'emms-pause))

(use-package mpv
  :ensure t)

;; tab-bar history
;; (tab-bar-history-mode 1)
(global-set-key (kbd "M-[") #'winner-undo)
(global-set-key (kbd "M-]") #'winner-redo)

(defun kf/open-or-close-tab (prefix)
    "Open a new tab, close with a prefix"
    (interactive "P")
    (if prefix
        (tab-close)
      (tab-new)))

(put 'upcase-region 'disabled nil)

(use-package dot-mode)

(use-package calc
  :defer t)


(defun kf/ispell-norwegian ()
  "Change ispell dictionary to Norwegian."
  (interactive)
  (ispell-change-dictionary "no" nil))

(defun kf/ispell-english ()
  "Change ispell dictionary to English."
  (interactive)
  (ispell-change-dictionary "en" nil))

(defun kf/ispell-estonian ()
  "Change ispell dictionary to Estonian"
  (interactive)
  (ispell-change-dictionary "et" nil))

;; (setq ispell-program-name "aspell")


(global-set-key (kbd "<f12>") #'kf/ispell-norwegian)
(global-set-key (kbd "C-<f12>") #'kf/ispell-estonian)
(global-set-key (kbd "<f11>") #'kf/ispell-english)

(use-package expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

(use-package eat
  :ensure t
  :config
  (customize-set-variable 'eat-term-scrollback-size nil))

;; For `eat-eshell-mode'.
(add-hook 'eshell-load-hook #'eat-eshell-mode)

;; For `eat-eshell-visual-command-mode'.
(add-hook 'eshell-load-hook #'eat-eshell-visual-command-mode)


(use-package fd-dired
  :ensure t)

;; very nice fuzzy completion, but slows down vertico a lot
(customize-set-variable 'fido-mode nil)

;; LaTeX stuff
;; Make sure pdfs open up with pdf-tools instead of trying evince
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)

(add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)


(customize-set-variable 'ring-bell-function 'ignore)

(use-package popper
  :ensure t ; or :straight t
  :bind (("C-`"   . popper-toggle)
         ("M-`"   . popper-cycle)
         ("C-M-`" . popper-toggle-type))
  :init
  (setq popper-reference-buffers
        '("\\*Messages\\*"
          "Output\\*$"
          "\\*Async Shell Command\\*"
          help-mode
          compilation-mode))
  (popper-mode +1)
  (popper-echo-mode +1))                ; For echo area hints

(server-start)

(use-package khardel
  :ensure t)

;; From https://codeberg.org/rahguzar/wile/src/branch/main/wile.el
;; Control iwd with wile!
(load-file "~/.config/emacs/wile.el")
;; Control bluez with bile
(load-file "~/.config/emacs/bile.el")

;; Clamps stuff
;; save buffers and invoke fomus in the default Lisp
(setq fomus-args "")
(defun run-fomus ()
  (interactive)
  (save-some-buffers)
  (let ((a (read-from-minibuffer "FOMUS arguments: " fomus-args)))
    (setq fomus-args a)
    (sly-interactive-eval (format "(fomus %S %s)" buffer-file-name a))))
;; (keymap-set sly-mode-map "C-c C-o" 'run-fomus)

(defun kill-thing-at-point (thing)
  "Kill the `thing-at-point' for the specified kind of THING."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (if bounds
        (kill-region (car bounds) (cdr bounds))
      (error "No %s at point" thing))))

(defun kill-word-at-point ()
  "Kill the word at point."
  (interactive)
  (kill-thing-at-point 'word))

(global-set-key (kbd "C-c M-k") 'kill-word-at-point)

; rss reader
(use-package elfeed
  :ensure t
  :custom
  (elfeed-feeds
   '(
     ;; programming
     ("https://news.ycombinator.com/rss" hacker)
     ("https://www.reddit.com/r/programming.rss" programming)
     ("https://www.reddit.com/r/emacs.rss" emacs)

     ("http://rss.cnn.com/rss/cnn_latest.rss/" cnn)
     ("https://theguardian.com/rss" the-guardian)
     ("https://www.nrk.no/rss/" nrk)
     ("https://fixthenews.com/rss" fix-the-news)
     ))
  (elfeed-search-filter "@2-days-ago +unread")
  (elfeed-search-title-max-width 100)
  (elfeed-search-title-min-width 100))

(global-completion-preview-mode 1)
;; From here: https://eshelyaron.com/posts/2023-11-17-completion-preview-in-emacs.html

;; Bindings that take effect when the preview is shown:
(with-eval-after-load 'completion-preview
  ;; Org mode has a custom `self-insert-command'
  (push 'org-self-insert-command completion-preview-commands)
  ;; Paredit has a custom `delete-backward-char' command
  (push 'paredit-backward-delete completion-preview-commands)

  ;; Cycle the completion candidate that the preview shows
  (define-key completion-preview-active-mode-map (kbd "M-n") #'completion-preview-next-candidate)
  (define-key completion-preview-active-mode-map (kbd "M-p") #'completion-preview-prev-candidate))


(use-package yasnippet-capf
  :ensure t
  :after cape
  :config
  (add-to-list 'completion-at-point-functions #'yasnippet-capf))

;; (add-hook 'prog-mode-hook
;;           (lambda ()
;;             (add-to-list 'completion-at-point-functions #'yasnippet-capf)))

;; (add-hook 'lisp-mode-hook
;;           (lambda ()
;;             (add-to-list 'completion-at-point-functions #'yasnippet-capf)))

;; (add-hook 'text-mode-hook
;;           (lambda ()
;;             (add-to-list 'completion-at-point-functions #'yasnippet-capf)))

(use-package consult-yasnippet
  :ensure t
  :bind (("C-c C-<SPC>" . #'consult-yasnippet)))

(use-package common-lisp-snippets
  :ensure t
  :after yas
  :hook lisp-mode-hook)



(provide 'init)
;; init.el ends here
