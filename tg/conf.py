import os

def get_pass(key):
    # retrieves key from password store
    return os.popen("pass show {} | head -n 1".format(key)).read().strip()

PHONE = get_pass("kf/telephone")
